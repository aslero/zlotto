<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();


Route::get('lang/{locale}', function ($locale) {
    \Illuminate\Support\Facades\App::setLocale($locale);
    Session::put('locale', $locale);                    # И устанавливаем его в сессии под именем locale
    return redirect()->back();                              # Редиректим его <s>взад</s> на ту же страницу

})->name('lang');

Route::get('login/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('login/{provider}/callback', 'Auth\LoginController@handleProviderCallback');


Route::group(['namespace' => 'Admin','prefix' => 'admin','middleware' => ['auth']], function () {
    Route::get('/', 'DashboardController@index')->name('admin');
    Route::get('/modules', 'ModuleController@index')->name('admin.modules');
    Route::get('/modules/poller', 'PollerController@index')->name('admin.poller');
    Route::post('/modules/poller/option', 'PollerController@option');
    Route::post('/modules/poller/option/store', 'PollerController@storeOption');
    Route::post('/modules/poller/option/delete', 'PollerController@deleteOption');
    Route::get('/modules/poller/show', 'PollerController@show');
    Route::post('/modules/poller/store', 'PollerController@store');
    Route::post('/modules/poller/update', 'PollerController@update');
    Route::post('/modules/poller/delete', 'PollerController@delete');
    Route::post('/modules/poller/published', 'PollerController@published');
    Route::get('/logout', 'DashboardController@logout')->name('admin.logout');
});


Route::get('/', 'HomeController@index')->name('home');
Route::get('/how-it-works', 'Frontend\AboutController@index')->name('about');
Route::get('/pricing', 'Frontend\TariffController@index')->name('pricing');
Route::get('/copyright', 'Frontend\CopyrightController@index')->name('copyright');

Route::group(['namespace' => 'Profile','prefix' => 'profile','middleware' => ['auth']], function () {
    Route::get('/', 'ProfileController@index')->name('profile');
    Route::get('/settings', 'SettingController@index')->name('settings');
    Route::post('/settings', 'SettingController@update')->name('settings');
    Route::get('/histories', 'HistoryController@index')->name('histories');
    Route::get('/invitations', 'InvitationController@index')->name('invitations');
    Route::post('/invitations', 'InvitationController@send')->name('invitations');
    Route::get('/questionnaire', 'QuestionController@index')->name('questionnaire');
    Route::post('/questionnaire/store', 'QuestionController@store')->name('questionnaire.store');
    Route::get('/questionnaire/{step}', 'QuestionController@step')->name('questionnaire.step');
    Route::get('/choose-lottery', 'LotteryController@index')->name('lottery');
    Route::get('/logout', 'ProfileController@logout')->name('logout');
});
