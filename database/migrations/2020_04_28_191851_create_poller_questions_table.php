<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePollerQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('poller_questions', function (Blueprint $table) {
            $table->id();
            $table->string('text')->nullable();
            $table->string('slug')->nullable()->unique();
            $table->boolean('published')->default(true);
            $table->string('type')->nullable();
            $table->tinyInteger('step')->default(1);
            $table->boolean('required')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('poller_questions');
    }
}
