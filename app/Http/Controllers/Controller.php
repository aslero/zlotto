<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getUserAnswerQuestion($questions){
        foreach ($questions as $question) {
            $answer = '';
            $answerUser = $question->answers->where('user_id','=', Auth::user()->id)->where('qid','=', $question->id)->first();
            if ($answerUser){
                if ($answerUser->oid == 0) {
                    $question->answer = $answerUser->text;
                }else{
                    $optionQuestion = $question->options->where('id','=',$answerUser->oid)->first();

                    $question->answer = $optionQuestion['id'];
                }
            }else $question->answer = '';
        }
    }
}
