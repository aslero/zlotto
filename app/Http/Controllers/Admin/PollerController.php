<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Poller\PollerOption;
use App\Model\Poller\PollerQuestion;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class PollerController extends Controller
{
    public function index(){
        return view('admin.poller');
    }

    public function show(){
        return response()->json(PollerQuestion::orderBy('id','asc')->get());
    }

    public function create(){
        return view('admin.forms.poller-question');
    }

    public function update(Request $request){
        $errors_m = '';
        $v = Validator::make($request->all(), [
            'text' => 'required|max:255',
            'slug' => 'required',
            'step' => 'required',
        ]);

        if ($v->fails())
        {
            $errors = $v->errors();
            foreach ($errors->all() as $error){
                $errors_m.= $error;
            }
            return response()
                ->json([
                    'message' => $errors_m,
                    'error' => 1,
                ], 200);
        }

        $questionInBase = PollerQuestion::where('id','=', $request->id)->first();
        if ($questionInBase->type == 'select'){
            PollerOption::where('qid','=',$questionInBase->id)->delete();
        }

        $store = $questionInBase->update([
            'text' => trim($request->text),
            'slug' => trim($request->slug),
            'step' => $request->step,
            'type' => $request->type,
            'required' => $request->required,
            'updated_at' => Carbon::now(),
        ]);

        if ($store) return response()->json([
            'error' => 0,
            'message' => 'Successfully updated!',
            'question' => PollerQuestion::where('id','=', $request->id)->first()
        ]);else return response()->json([
            'error' => 1,
            'message' => 'Oops! Something went wrong! Try again!'
        ]);

    }

    public function store(Request $request){
        $errors_m = '';
        $v = Validator::make($request->all(), [
            'text' => 'required|max:255',
            'slug' => 'required',
            'step' => 'required',
        ]);

        if ($v->fails())
        {
            $errors = $v->errors();
            foreach ($errors->all() as $error){
                $errors_m.= $error;
            }
            return response()
                ->json([
                    'message' => $errors_m,
                    'error' => 1,
                ], 200);
        }

        $store = PollerQuestion::create([
            'text' => trim($request->text),
            'slug' => trim($request->slug),
            'step' => $request->step,
            'type' => $request->type,
            'required' => $request->required,
            'published' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        if ($store) return response()->json([
            'error' => 0,
            'message' => 'Successfully added!',
            'question' => $store
        ]);else return response()->json([
            'error' => 1,
            'message' => 'Oops! Something went wrong! Try again!'
        ]);

    }

    public function delete(Request $request){
        $question = PollerQuestion::findOrFail($request->id);
        if ($question){
            $delete = $question->delete();

            if ($delete) return response()->json([
                'error' => 0,
                'message' => 'Successfully deleted!'
            ]);else return response()->json([
                'error' => 1,
                'message' => 'Oops! Something went wrong! Try again!'
            ]);

        }else return response()->json([
            'error' => 1,
            'message' => 'Not question'
        ]);
    }

    public function published(Request $request){
        if ($request->isMethod('post')){
            if (Auth::check()){

                $published = PollerQuestion::where('id', '=', $request->id)->first()->update([
                    'published' => $request->type
                ]);

                if ($request->type == 0){
                    if ($published) return response()->json([
                        'error' => 0,
                        'message' => "Discontinued!"
                    ]);else return response()->json([
                        'error' => 1,
                        'message' => "Oops! Something went wrong! Try again!"
                    ]);
                }else{
                    if ($published) return response()->json([
                        'error' => 0,
                        'message' => "Published!"
                    ]);else return response()->json([
                        'error' => 1,
                        'message' => "Oops! Something went wrong! Try again!"
                    ]);
                }

            }else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }

    }
    public function option(Request $request){
        return response()->json(PollerOption::where('qid','=', $request->id)->get());
    }

    public function storeOption(Request $request){
        $errors_m = '';
        $v = Validator::make($request->all(), [
            'option' => 'required|max:255',
        ]);

        if ($v->fails())
        {
            $errors = $v->errors();
            foreach ($errors->all() as $error){
                $errors_m.= $error;
            }
            return response()
                ->json([
                    'message' => $errors_m,
                    'error' => 1,
                ], 200);
        }

        $store = PollerOption::create([
            'option' => trim($request->option),
            'qid' => $request->qid,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        if ($store) return response()->json([
            'error' => 0,
            'message' => 'Succeffull',
            'option' => $store
        ]);else return response()->json([
            'error' => 1,
            'message' => 'Oops! Something went wrong! Try again!'
        ]);

    }

    public function deleteOption(Request $request){
        $question = PollerOption::findOrFail($request->id);
        if ($question){
            $delete = $question->delete();

            if ($delete) return response()->json([
                'error' => 0,
                'message' => 'Successfully deleted!'
            ]);else return response()->json([
                'error' => 1,
                'message' => 'Oops! Something went wrong! Try again!'
            ]);

        }else return response()->json([
            'error' => 1,
            'message' => 'Not question'
        ]);
    }
}




