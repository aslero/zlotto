<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Model\Poller\PollerAnswer;
use App\Model\Poller\PollerQuestion;
use App\Model\QuestionStep;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class QuestionController extends Controller
{
    public function index(){

        $questions = PollerQuestion::where('step','=', 1)->with('answers')->get();

        $this->getUserAnswerQuestion($questions);

        return view('profile.questionnaire',[
            'questions' => $questions
        ]);
    }
    public function step($step){
        $questions = PollerQuestion::where('step','=', $step)->get();

        $this->getUserAnswerQuestion($questions);

        return view('profile.questionnaire-'.$step,[
            'questions' => $questions
        ]);
    }

    public function store(Request $request){
        $i = 0;
        foreach ($request->step as $key=>$item) {
            $oid = 0;
            $question = PollerQuestion::where('id','=', $key)->first();
            if ($question->type == 'select'){
                $option = $question->options->where('id','=', $item)->first();
                if ($option) $oid = $option->id;
            }else{
                $oid = 0;
            }
            $store = PollerAnswer::create([
                'qid' => $key,
                'oid' => $oid,
                'text' => $item,
                'user_id' => Auth::user()->id,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
            if ($store) $i = $i + 1;
        }
        if ($i > 0){
            $step = QuestionStep::where('user_id', Auth::user()->id)->first();

            $step->update([
                'step' => $request->position,
                'updated_at' => Carbon::now(),
            ]);
            if ($request->position < 3){
                $j = $request->position + 1;
                return redirect()->route('questionnaire.step', $j);
            }else{
                return redirect()->route('profile');
            }
        }else return redirect()->back();
    }
}
