<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Model\UserNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class SettingController extends Controller
{
    public function index(){
        return view('profile.settings');
    }

    public function update(Request $request){
        if ($request->isMethod('POST')){

            $v = Validator::make($request->all(), [
                'name' => ['required', 'string', 'max:255'],
                'first_name' => ['required', 'string', 'max:255'],
                'last_name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'max:255','email'],
                'pass' => ['required'],
                'birth' => ['required'],
            ]);

            if ($v->fails())
            {
                return redirect()->back()->withErrors($v->errors())->withInput();
            }

            if (!empty($request->new_pass) && !empty($request->confirm_pass)){
                if ($request->new_pass != $request->confirm_pass){

                }else{
                    $password = $request->new_pass;
                }
            }else{
                $password = $request->pass;
            }

            $update = Auth::user()->update([
                'name' => htmlspecialchars(trim($request->name)),
                'first_name' => htmlspecialchars(trim($request->first_name)),
                'last_name' => htmlspecialchars(trim($request->last_name)),
                'birth' => htmlspecialchars(trim($request->birth)),
                'pass' => $password,
                'password' => Hash::make($password),
                'updated_at' => Carbon::now()
            ]);

            if ($update){
                $notif_mail = 0;
                if ($request->notif_mail) $notif_mail = 1;
                UserNotification::where('user_id','=', Auth::user()->id)->first()->update([
                    'mail' => $notif_mail
                ]);
            }

            return redirect()->back();

        }else return redirect()->route('home');
    }
}
