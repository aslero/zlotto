<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Jobs\SendInvitation;
use App\Mail\EmailInvitation;
use App\Model\Invitation;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class InvitationController extends Controller
{
    public function index(){
        return view('profile.invitation');
    }

    public function send(Request $request)
    {
        if ($request->isMethod('POST')) {
            $v = Validator::make($request->all(), [
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'max:255', 'email'],
            ]);

            if ($v->fails()) {
                return redirect()->back()->withErrors($v->errors())->withInput();
            }

            $store = Invitation::create([
                'user_id' => Auth::user()->id,
                'name' => htmlspecialchars(trim($request->name)),
                'email' => trim($request->email),
                'status' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);

            if ($store){
                SendInvitation::dispatch(trim($request->email), htmlspecialchars(trim($request->name)),Auth::user())->delay(Carbon::now()->addMinute());
            }

            return redirect()->back();

        }else return redirect()->back();
    }
}
