<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Model\QuestionStep;
use App\Model\UserNotification;
use App\Providers\RouteServiceProvider;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */

    public function register(Request $request)
    {
        $errors_m = '';


        $v = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255', 'unique:users'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6'],
        ]);

        if ($v->fails())
        {
            $errors = $v->errors();
            foreach ($errors->all() as $error){
                $errors_m.= $error;
            }
            return response()
                ->json([
                    'message' => $errors_m,
                    'error' => 1,
                ], 200);
        }

        $user =  User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'pass' => $request->password,
            'ip' => $_SERVER['REMOTE_ADDR'],
            'parent' => 0
        ]);

        if ($user){
            UserNotification::create([
                'user_id' => $user->id,
                'mail' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);

            QuestionStep::create([
                'user_id' => $user->id,
                'step'=> 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }
        if (Auth::attempt(array('name' => $request->name, 'password' => $request->password))){
            return response()->json([
                'error' => 0,
                'message' => 'Registration successful!',
                'redirect' => $this->redirectTo
            ]);
        }
        return response()->json([
            'error' => 1,
            'message' => 'These credentials do not match our records!'
        ]);
    }

   /* protected function create(array $data)
    {
        $user =  User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'pass' => $data['password'],
            'ip' => $_SERVER['REMOTE_ADDR'],
            'parent' => 0
        ]);

        if ($user){
            UserNotification::create([
                'user_id' => $user->id,
                'mail' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);

            QuestionStep::create([
                'user_id' => $user->id,
                'step'=> 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }

        return $user;

    }*/
}
