<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Model\QuestionStep;
use App\Model\UserNotification;
use App\Provider;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::PROFILE;


    /**
     * Login username to be used by the controller.
     *
     * @var string
     */
    protected $username;

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');

        $this->username = $this->findUsername();
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function findUsername()
    {
        $login = request()->input('login');

        $fieldType = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'name';

        request()->merge([$fieldType => $login]);

        return $fieldType;
    }

    /**
     * Get username property.
     *
     * @return string
     */
    public function username()
    {
        return $this->username;
    }


    public function login(Request $request){
        $login = request()->input('login');

        $fieldType = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'name';

        request()->merge([$fieldType => $login]);



        $password = $request->input('password');
        if (Auth::attempt(array($fieldType => $login, 'password' => $password))){
            return response()->json([
                'error' => 0,
                'message' => 'Login successful!',
                'redirect' => $this->redirectTo
            ]);
        }
        return response()->json([
            'error' => 1,
            'message' => 'These credentials do not match our records!'
        ]);
    }

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }



    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($provider)
    {
        $login = '';
        $providerUser = Socialite::driver($provider)->user();


        $user = User::where('email','=', $providerUser->email)->first();
        if ($user){
            $userProvider = Provider::where('user_id','=',$user->id)
                ->where('provider_id','=', $providerUser->id)
                ->first();
            if (!$userProvider){
                Provider::create([
                    'user_id' => $user->id,
                    'provider_id' => $providerUser->getId(),
                    'provider' => $provider,
                    'nickname' => $providerUser->getNickname(),
                    'name' => $providerUser->getName(),
                    'email' => $providerUser->getEmail(),
                    'avatar' => $providerUser->getAvatar(),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);
            }

        }else{
            if (empty($providerUser->getNickname())) $login = $providerUser->getEmail();else $login = $providerUser->getNickname();
            $randPassword = Str::random(6);
            $user = User::create([
                'name' => $login,
                'email' => $providerUser->getEmail(),
                'parent' => 0,
                'password' => Hash::make($randPassword),
                'pass' => $randPassword,
                'ip' => $_SERVER['REMOTE_ADDR'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);

            if ($user){
                Provider::create([
                    'user_id' => $user->id,
                    'provider_id' => $providerUser->getId(),
                    'provider' => $provider,
                    'nickname' => $providerUser->getNickname(),
                    'name' => $providerUser->getName(),
                    'email' => $providerUser->getEmail(),
                    'avatar' => $providerUser->getAvatar(),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);

                UserNotification::create([
                    'user_id' => $user->id,
                    'mail' => 1,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);

                QuestionStep::create([
                    'user_id' => $user->id,
                    'step'=> 0,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ]);
            }
        }
        Auth::login($user,true);

        return redirect($this->redirectTo);
    }
}
