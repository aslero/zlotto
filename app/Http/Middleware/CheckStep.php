<?php

namespace App\Http\Middleware;

use Closure;

class CheckStep
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()->steps->step == 0) return $next($request);
        if ($request->user()->steps->step > 0 && $request->user()->steps->step< 3) return redirect()->route('questionnaire.step',$request->user()->steps->step+1);
        if ($request->user()->steps->step == 3) return redirect()->route('questionnaire.profile');
    }
}
