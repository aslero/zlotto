<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserNotification extends Model
{
    protected $table = "user_notifications";

    protected $fillable = [
        'user_id', 'mail'
    ];
}
