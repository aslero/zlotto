<?php

namespace App\Model\Poller;

use App\Model\Invitation;
use Illuminate\Database\Eloquent\Model;

class PollerQuestion extends Model
{
    protected $table = "poller_questions";

    protected $fillable = [
        'text','slug','published','type','step','required'
    ];

    public function options(){
        return $this->hasMany(PollerOption::class,'qid');
    }

    public function answers(){
        return $this->hasMany(PollerAnswer::class,'qid');
    }
}
