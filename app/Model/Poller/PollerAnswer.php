<?php

namespace App\Model\Poller;

use Illuminate\Database\Eloquent\Model;

class PollerAnswer extends Model
{
    protected $table = "poller_answers";

    protected $fillable = [
        'user_id','qid','oid','text'
    ];
}
