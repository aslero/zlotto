<?php

namespace App\Model\Poller;

use Illuminate\Database\Eloquent\Model;

class PollerOption extends Model
{
    protected $table = "poller_options";

    protected $fillable = [
        'qid','option'
    ];

    public function answer(){
        return $this->hasMany(PollerAnswer::class,'oid');
    }
}
