<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Invitation extends Model
{
    protected $table = "invitations";

    protected $fillable = [
        'name','email','status','user_id'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
