<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class QuestionStep extends Model
{
    protected $table = "question_steps";

    protected $fillable = [
        'user_id', 'step'
    ];
}
