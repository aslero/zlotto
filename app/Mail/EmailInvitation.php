<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmailInvitation extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $mail;
    protected $name;
    protected $user;

    public function __construct($mail,$name,$user)
    {
        $this->mail = $mail;
        $this->name = $name;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.invitation',[
            'mail' => $this->mail,
            'name' => $this->name,
            'user' => $this->user,
        ]);
    }
}
