<div class="sidebar">
    <nav class="menu">
        <a href="/admin/" class="menu__item {{ Request::is('admin') ? 'active' : '' }}"  >

            <div class="menu__item-text">
                <i class="fas fa-tachometer-alt"></i>
            </div>
        </a>
        <a href="{{ route('admin.modules') }}" class="menu__item {{ Request::is('admin/modules') ? 'active' : '' }}" >
            <div class="menu__item-text">
                <i class="fas fa-th"></i>
                <div class="menu__item-label">Modules</div>
            </div>
        </a>
    </nav>
</div>
