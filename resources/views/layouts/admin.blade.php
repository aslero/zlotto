<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <script src="https://kit.fontawesome.com/f343de9ed5.js" crossorigin="anonymous"></script>
    <script src="{{ asset('js/admin.js') }}"defer></script>
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
</head>
<body>
<div id="app">
    <div class="sc-background"></div>
    <header>
        <div class="container-fluid">
            <div class="box__header">
                <div class="header--title">
                    <h1>@yield('h1')</h1>
                </div>
                <div class="user_action-box">
                    <div class="header__user">
                        @if (Auth::check())
                            <div class="user__data dropdown">
                                <button class="user_header dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <p class="username">{{ Auth::user()->name }} <span class="carret"></span></p>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                    <ul class="user__nav">
                                        <li><a href="{{ route('admin.logout') }}">Close</a></li>
                                    </ul>
                                </div>
                            </div>
                        @endif
                        <div class="mobilem" id="mobilem">
                            <span class="_line __1"></span>
                            <span class="_line __2"></span>
                            <span class="_line __3"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    @if (Auth::check())
        @include('layouts.admin.sidebar')
    @endif
    <div class="main">
        @if(\Illuminate\Support\Facades\Auth::guest())
            @yield('content')
        @else
            <div class="content__wrapper">
                <div class="wrapper">
                    @yield('content')
                </div>
            </div>
        @endif
    </div>
</div>
</body>
</html>
