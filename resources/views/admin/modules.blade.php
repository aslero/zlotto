@extends('layouts.admin')
@section('h1','Modules')
@section('content')

    <div class="row">
        <div class="col-lg-2">
            <div class="panel">
                <div class="panel--header">
                    <p class="title">Modules</p>
                </div>
                <div class="panel-body">
                    <a href="{{ route('admin.poller') }}" class="panel--link" >
                        <div class="panel--link-text">
                            <i class="fas fa-question"></i>
                            <div class="panel--link-label">Edit</div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
