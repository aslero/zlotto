@extends('layouts.app')

@section('content')
    <div class="paym">

        <div class="wrapper">

            <div class="paym__content">

                <h1>@lang('pricing.title')</h1>

                <div class="paym__side">

                    <div class="paym__price">
                        <div class="paym__sale">
                            <div class="paym__sale-text"><span><span>-<b>39</b></span>%</span>@lang('pricing.discount')</div>
                        </div>
                        <div class="paym__price-old"><span>$ 39.99</span></div>
                        <div class="paym__price-new">$ 24.99</div>
                        <div class="paym__price-sign">@lang('pricing.p_year')</div>
                        <div class="paym__price-text">@lang('pricing.description')</div>
                        <div class="paym__sl paym__sl_months" data-min="1" data-max="3" data-val="3" data-type="months">
                            <div class="paym__sl-list">
                                <div class="paym__sl-item paym__sl-item_active" data-val="1" data-price="24.99" data-discount="19">3 @lang('pricing.month')</div>
                                <div class="paym__sl-item paym__sl-item_active" data-val="2" data-price="25.99" data-discount="29">6 @lang('pricing.month')</div>
                                <div class="paym__sl-item paym__sl-item_active" data-val="3" data-price="26.99" data-discount="39">12 @lang('pricing.month')</div>
                            </div>
                        </div>
                    </div>

                    <div class="paym__item paym__item_1month">
                        <div class="paym__item-top">
                            <div class="paym__item-top-l">@lang('pricing.m_package')</div>
                            <div class="paym__item-top-v">$19.99</div>
                        </div>
                        <label class="paym__item-check">
                            <span class="paym__item-check-l">@lang('pricing.unlimited')</span>
                            <span class="paym__item-check-v">
							<span class="checkbox">
								<input type="checkbox" class="checkbox__input paym-inp-1month"/><i class="ic-a"></i>
							</span>
						</span>
                        </label>
                    </div>

                    <div class="paym__sep"></div>

                    <div class="paym__item paym__item_member">
                        <div class="paym__item-top">
                            <div class="paym__item-top-l">@lang('pricing.add_f_m')</div>
                            <div class="paym__item-top-v">$14.99</div>
                        </div>
                        <div class="paym__item-check">
                            <div class="paym__item-check-l">@lang('pricing.add_f_d')</div>
                            <div class="paym__item-check-v">
							<span class="checkbox">
								<input type="checkbox" class="checkbox__input paym-inp-members"/><i class="ic-a"></i>
							</span>
                            </div>
                        </div>
                        <div class="paym__sl paym__sl_members" data-min="1" data-max="4" data-val="2" data-type="members">
                            <div class="paym__sl-list">
                                <div class="paym__sl-item paym__sl-item_active" data-val="1">1</div>
                                <div class="paym__sl-item paym__sl-item_active" data-val="2">2</div>
                                <div class="paym__sl-item" data-val="3">3</div>
                                <div class="paym__sl-item" data-val="4">4</div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="paym__app">

                    <form action="#">

                        <div class="paym__type">

                            <div class="paym__type-item">
                                <label class="paym__type-label">
                                    <input type="radio" name="type" class="paym__type-input" checked="checked"/>
                                    <span class="paym__type-logo">
									<img src="i/pay/mastercard.png" alt=""/>
								</span>
                                </label>
                            </div>

                            <div class="paym__type-item">
                                <label class="paym__type-label">
                                    <input type="radio" name="type" class="paym__type-input"/>
                                    <span class="paym__type-logo">
									<img src="i/pay/visa.png" alt=""/>
								</span>
                                </label>
                            </div>

                        </div>

                        <div class="paym__stat">

                            <div class="paym__stat-i paym__stat-i_months">
                                <div class="paym__stat-l">@lang('pricing.tariff')</div>
                                <div class="paym__stat-v">12 @lang('pricing.month')</div>
                            </div>

                            <div class="paym__stat-i paym__stat-i_members hidden">
                                <div class="paym__stat-l">@lang('pricing.members')</div>
                                <div class="paym__stat-v">2</div>
                            </div>

                            <div class="paym__stat-i paym__stat-i_total">
                                <div class="paym__stat-l">@lang('pricing.total')</div>
                                <div class="paym__stat-v">$ 50.99</div>
                            </div>

                        </div>

                        <div class="paym__form">

                            <div class="form-item paym__form-item">
                                <div class="form-item__level">@lang('pricing.card_number')</div>
                                <div class="form-item__value">
                                    <input type="text" class="input form-item__input input-card" placeholder="4111 1111 1111 1111"/>
                                    <div class="input-warning">@lang('pricing.error_card_number')</div>
                                </div>
                            </div>

                            <div class="form-item paym__form-item">
                                <div class="form-item__level">@lang('pricing.expiration_date')</div>
                                <div class="form-item__value">
                                    <input type="text" class="input form-item__input input-expd" placeholder="MM/YY"/>
                                    <div class="input-warning">@lang('pricing.error_expiration_date')</div>
                                </div>
                            </div>

                            <div class="form-item paym__form-item">
                                <div class="form-item__level">@lang('pricing.cvv')</div>
                                <div class="form-item__value">
                                    <input type="text" class="input form-item__input input-cvv" placeholder="XXX"/>
                                    <div class="input-warning">@lang('pricing.error_cvv')</div>
                                </div>
                            </div>

                            <div class="form-check paym__form-check">
                                <label class="form-check__label">
								<span class="checkbox form-check__checkbox">
									<input type="checkbox" class="checkbox__input pp" checked="checked"/><i class="ic-a"></i>
								</span>@lang('buttons.terms')
                                </label>
                            </div>

                            <div class="paym__bt">
                                <button type="submit" class="btn paym__btn">@lang('buttons.byu_now')</button>
                            </div>

                        </div>

                        <div class="paym__img">
                            <img src="i/paym.svg" alt=""/>
                        </div>

                    </form>

                </div>

            </div>

        </div>

    </div>
@endsection
