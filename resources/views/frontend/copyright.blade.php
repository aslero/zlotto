@extends('layouts.app')

@section('content')
    <div class="copyright">

        <div class="wrapper copyright__wrapper">

            <div class="copyright__content">

                <h1>Copyright</h1>

                <div class="copyright__text">
                    Copyright &copy; 2010 by <span class="dib">Bill Shakespeare</span>
                    <br/>
                    <br/>
                    All rights reserved. No part of this publication may be reproduced, distributed, or transmitted in any form or by any means, including photocopying, recording, or other electronic or mechanical methods, without the prior written permission of the publisher, except in the case of brief quotations embodied in critical reviews and certain other noncommercial uses permitted by copyright law.
                </div>

            </div>

        </div>

    </div>
@endsection
