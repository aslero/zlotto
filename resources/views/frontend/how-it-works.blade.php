@extends('layouts.app')

@section('content')
    <div class="section how how-1" id="how1" data-anchor="how1">
        <div class="how-line">
            <div class="how-line__point"></div>
        </div>
        <div class="how__wrapper wrapper">
            <div class="how__entry">
                <div class="how__inner">
                    <div class="how__number">01</div>
                    <div class="how__title">@lang('about.step_1')</div>
                </div>
            </div>
            <div class="how__img">
                <img src="/i/how/1.svg" alt=""/>
            </div>
        </div>
    </div>
    <div class="section how how-2" data-anchor="how2">
        <div class="how-line">
            <div class="how-line__point"></div>
        </div>
        <div class="how__wrapper wrapper">
            <div class="how__entry">
                <div class="how__inner">
                    <div class="how__number">02</div>
                    <div class="how__title">@lang('about.step_2')</div>
                </div>
            </div>
            <div class="how__img">
                <img src="/i/how/2.svg" alt=""/>
            </div>
        </div>
    </div>
    <div class="section how how-3" data-anchor="how3">
        <div class="how-line">
            <div class="how-line__point"></div>
        </div>
        <div class="how__wrapper wrapper">
            <div class="how__entry">
                <div class="how__inner">
                    <div class="how__number">03</div>
                    <div class="how__title">@lang('about.step_3')</div>
                </div>
            </div>
            <div class="how__img">
                <img src="/i/how/3.svg" alt=""/>
            </div>
        </div>
    </div>
    <div class="section how how-4" data-anchor="how4">
        <div class="how-line">
            <div class="how-line__point"></div>
        </div>
        <div class="how__wrapper wrapper">
            <div class="how__entry">
                <div class="how__inner">
                    <div class="how__number">04</div>
                    <div class="how__title">@lang('about.step_4')</div>
                </div>
            </div>
            <div class="how__img">
                <img src="/i/how/4.svg" alt=""/>
            </div>
        </div>
    </div>

    <div class="section how how-5" data-anchor="how5">

        <div class="how-line">
            <div class="how-line__point"></div>
        </div>

        <div class="how__wrapper wrapper">

            <div class="how__entry">

                <div class="how__inner">

                    <div class="how__number">05</div>
                    <div class="how__title">@lang('about.step_5')</div>

                </div>

            </div>

            <div class="how__img">
                <img src="/i/how/5.svg" alt=""/>
            </div>

        </div>

    </div><!-- .how-5 -->

    <div class="section how how-6" data-anchor="how6">

        <div class="how-line">
            <div class="how-line__point"></div>
        </div>

        <div class="how__wrapper wrapper">

            <div class="how__entry">

                <div class="how__inner">

                    <div class="how__number">06</div>
                    <div class="how__title">@lang('about.step_6')</div>

                </div>

            </div>

            <div class="how__img">
                <img src="/i/how/6.svg" alt=""/>
            </div>

        </div>

    </div><!-- .how-6 -->

    <div class="section how how-7" data-anchor="how7">

        <div class="how-line">
            <div class="how-line__point"></div>
        </div>

        <div class="how__wrapper wrapper">

            <div class="how__entry">

                <div class="how__inner">

                    <div class="how__number">07</div>
                    <div class="how__title">@lang('about.step_7')</div>

                </div>

            </div>

            <div class="how__img">
                <img src="/i/how/7.svg" alt=""/>
            </div>

        </div>

    </div><!-- .how-7 -->

    <div class="section how-end" data-anchor="howend">

        <div class="how-line"></div>

        <div class="wrapper how-end__wrapper">

            <div class="how-end__logo" id="lottie"></div>
            <div class="how-end__logo-text"></div>

            <div class="how-end__tc">
                <label class="how-end__tc-label">
				<span class="checkbox how-end__tc-checkbox">
					<input type="checkbox" class="checkbox__input tc"/><i class="ic-a"></i>
				</span><u>@lang('buttons.terms')</u>
                </label>
            </div>

            <div class="how-end__bt">
                <a href="" class="btn how-end__btn">@lang('buttons.create_account')</a>
            </div>

        </div>

    </div>


    <div class="limited">
        <div class="limited__inn">
            <div class="limited__text">Spots are <span>LIMITED</span></div>
        </div>
    </div>

@endsection
