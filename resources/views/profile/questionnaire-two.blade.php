@extends('layouts.app')

@section('content')
    <div class="cab-page">
        <div class="wrapper cab-page__wrapper">
            @include('profile.partials.menu')
            <div class="cab-page__content">

                <h1>Questionnaire</h1>

                <div class="pg-steps pg-steps_quest">

                    <div class="pg-steps__item pg-steps__item_active">
                        <div class="pg-steps__line"></div>
                        <div class="pg-steps__numb"><span>1</span></div>
                    </div>

                    <div class="pg-steps__item pg-steps__item_cur pg-steps__item_show">
                        <div class="pg-steps__line"></div>
                        <div class="pg-steps__numb"><span>2</span></div>
                    </div>

                    <div class="pg-steps__item">
                        <div class="pg-steps__line"></div>
                        <div class="pg-steps__numb"><span>3</span></div>
                    </div>

                </div><!-- .pg-steps -->

                <div class="quest">

                    <div class="quest__form">

                        <div class="quest__item">
                            <div class="quest__item-lvl">What is your favorite month of the year?</div>
                            <div class="quest__item-val">
                                <input type="text" class="input quest__input" placeholder="Placeholder"/>
                            </div>
                        </div>

                        <div class="quest__item">
                            <div class="quest__item-lvl">What is the month of the year you dislike the most?</div>
                            <div class="quest__item-val">
                                <input type="text" class="input quest__input" placeholder="Culpa qui"/>
                            </div>
                        </div>

                        <div class="quest__item">
                            <div class="quest__item-lvl">What is your mother’s date of birth?</div>
                            <div class="quest__item-val">
                                <input type="text" class="input quest__input" placeholder="Placeholder"/>
                            </div>
                        </div>

                        <div class="quest__item">
                            <div class="quest__item-lvl">Are you married?</div>
                            <div class="quest__item-val">
                                <input type="text" class="input quest__input" placeholder="Placeholder"/>
                            </div>
                        </div>

                        <div class="quest__item">
                            <div class="quest__item-lvl">If you have a spouse, what is his or hers date of birth?</div>
                            <div class="quest__item-val">
                                <input type="text" class="input quest__input" placeholder="Placeholder"/>
                            </div>
                        </div>

                        <div class="quest__item">
                            <div class="quest__item-lvl">What was your wedding date?</div>
                            <div class="quest__item-val">
                                <input type="text" class="input quest__input" placeholder="Placeholder"/>
                            </div>
                        </div>

                        <div class="quest__item">
                            <div class="quest__item-lvl">Do you have children?</div>
                            <div class="quest__item-val">
                                <input type="text" class="input quest__input" placeholder="Placeholder"/>
                            </div>
                        </div>

                        <div class="quest__item">
                            <div class="quest__item-lvl">How many kids do you have?</div>
                            <div class="quest__item-val">
                                <input type="text" class="input quest__input" placeholder="Culpa qui"/>
                            </div>
                        </div>

                        <div class="quest__item">
                            <div class="quest__item-lvl">What is the birthday of your oldest child?</div>
                            <div class="quest__item-val">
                                <input type="text" class="input quest__input" placeholder="Culpa qui"/>
                            </div>
                        </div>

                        <div class="quest__bt">
                            <button type="submit" class="btn quest__btn">Next</button>
                        </div>

                    </div>

                </div><!-- .quest -->

            </div>
        </div>
    </div>
@endsection
