@extends('layouts.app')

@section('content')
    <div class="cab-page">
        <div class="wrapper cab-page__wrapper">
            @include('profile.partials.menu')
            <div class="cab-page__content">
                <h1>@lang('choose.title')</h1>
                <div class="pg-steps">
                    <div class="pg-steps__item pg-steps__item_cur pg-steps__item_show">
                        <div class="pg-steps__line"></div>
                        <div class="pg-steps__numb"><span>1</span></div>
                        <div class="pg-steps__text">@lang('choose.c_region')</div>
                    </div>

                    <div class="pg-steps__item">
                        <div class="pg-steps__line"></div>
                        <div class="pg-steps__numb"><span>2</span></div>
                        <div class="pg-steps__text">@lang('choose.lottery')</div>
                    </div>

                    <div class="pg-steps__item">
                        <div class="pg-steps__line"></div>
                        <div class="pg-steps__numb"><span>3</span></div>
                        <div class="pg-steps__text">@lang('choose.answer_question')</div>
                    </div>

                    <div class="pg-steps__item pg-steps__item_last">
                        <div class="pg-steps__line"></div>
                        <div class="pg-steps__numb"><span>4</span></div>
                    </div>
                </div>
                <div class="pg-top">
                    <h2>@lang('choose.c_country')&nbsp;&nbsp;<div class="tooltip" title="@lang('choose.c_country_t')">?</div></h2>
                </div>
                <div class="pg-country">
                    <div class="pg-country__list">
                        <div class="pg-country__item">
                            <label class="pg-country__label">
                                <input type="radio" name="country" class="pg-country__input" data-id="1"/>
                                <span class="pg-country__bg"></span>
                                <span class="pg-country__logo">
								<img src="/i/country/usa.svg" alt=""/>
							</span>
                                <span class="pg-country__text">United States of America</span>
                            </label>
                        </div>
                        <div class="pg-country__item">
                            <label class="pg-country__label">
                                <input type="radio" name="country" class="pg-country__input" data-id="2"/>
                                <span class="pg-country__bg"></span>
                                <span class="pg-country__logo">
								<img src="/i/country/canada.svg" alt=""/>
							</span>
                                <span class="pg-country__text">Canada</span>
                            </label>
                        </div>
                        <div class="pg-country__item">
                            <label class="pg-country__label">
                                <input type="radio" name="country" class="pg-country__input" data-id="3"/>
                                <span class="pg-country__bg"></span>
                                <span class="pg-country__logo">
								<img src="/i/country/australia.svg" alt=""/>
							</span>
                                <span class="pg-country__text">Australia</span>
                            </label>
                        </div>
                        <div class="pg-country__item">
                            <label class="pg-country__label">
                                <input type="radio" name="country" class="pg-country__input" data-id="4"/>
                                <span class="pg-country__bg"></span>
                                <span class="pg-country__logo">
								<img src="/i/country/gb.svg" alt=""/>
							</span>
                                <span class="pg-country__text">Great Britain</span>
                            </label>
                        </div>
                    </div>
                    <div class="pg-country__sel">
                        <select class="select">
                            <option disabled="disabled" selected="selected">Select</option>
                            <option data-id="1">United States of America</option>
                            <option data-id="2">Canada</option>
                            <option data-id="3">Australia</option>
                            <option data-id="4">Great Britain</option>
                        </select>
                    </div>
                    <div class="pg-region hidden">
                        <div class="pg-top">
                            <h2>@lang('choose.c_region')&nbsp;&nbsp;<div class="tooltip" title="@lang('choose.c_region_t')">?</div></h2>
                        </div>
                        <div class="pg-region__map" style="background: url(/files/map.jpg) center center no-repeat;"></div>
                        <div data-id="1" class="pg-region-sel">
                            <select class="select">
                                <option>United States of America 1</option>
                                <option>United States of America 2</option>
                                <option>United States of America 3</option>
                                <option>United States of America 4</option>
                                <option>United States of America 5</option>
                                <option>United States of America 1</option>
                                <option>United States of America 2</option>
                                <option>United States of America 3</option>
                                <option>United States of America 4</option>
                                <option>United States of America 5</option>
                            </select>
                        </div>
                        <div data-id="2" class="pg-region-sel hidden">
                            <select class="select">
                                <option>Canada 1</option>
                                <option>Canada 2</option>
                                <option>Canada 3</option>
                                <option>Canada 4</option>
                                <option>Canada 5</option>
                            </select>
                        </div>
                        <div data-id="3" class="pg-region-sel hidden">
                            <select class="select">
                                <option>Australia 1</option>
                                <option>Australia 2</option>
                                <option>Australia 3</option>
                                <option>Australia 4</option>
                                <option>Australia 5</option>
                            </select>
                        </div>
                        <div data-id="4" class="pg-region-sel hidden">
                            <select class="select">
                                <option>Great Britain 1</option>
                                <option>Great Britain 2</option>
                                <option>Great Britain 3</option>
                                <option>Great Britain 4</option>
                                <option>Great Britain 5</option>
                            </select>
                        </div>
                    </div>
                    <div class="pg-bt">
                        <button type="submit" class="btn pg-bt__btn">@lang('choose.submit')</button>
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection
