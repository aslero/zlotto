<div class="cab-page__sidebar">
    <div class="cab-menu">
        <div class="cab-menu__list">
            <div class="cab-menu__item {{ Request::is('profile/questionnaire') ? 'cab-menu__item_current' : '' }}">
                <a href="{{ route('questionnaire') }}">
							<span class="cab-menu__item-ic">
								<span class="icon-star"></span>
							</span>
                    <span class="cab-menu__item-tx">@lang('nav.profile.question')</span>
                </a>
            </div>
            <div class="cab-menu__item {{ Request::is('profile/invitations') ? 'cab-menu__item_current' : '' }}">
                <a href="{{ route('invitations') }}">
							<span class="cab-menu__item-ic">
								<span class="icon-myspace"></span>
							</span>
                    <span class="cab-menu__item-tx">@lang('nav.profile.add_relative')</span>
                </a>
            </div>
            <div class="cab-menu__item {{ Request::is('profile/histories') ? 'cab-menu__item_current' : '' }}">
                <a href="{{ route('histories') }}">
							<span class="cab-menu__item-ic">
								<span class="icon-job"></span>
							</span>
                    <span class="cab-menu__item-tx">@lang('nav.profile.past_number')</span>
                </a>
            </div>
            <div class="cab-menu__item {{ Request::is('profile/settings') ? 'cab-menu__item_current' : '' }}">
                <a href="{{ route('settings') }}">
							<span class="cab-menu__item-ic">
								<span class="icon-pie-chart"></span>
							</span>
                    <span class="cab-menu__item-tx">@lang('nav.profile.settings')</span>
                </a>
            </div>
            <div class="cab-menu__item {{ Request::is('profile') ? 'cab-menu__item_current' : '' }}">
                <a href="{{ route('profile') }}">
							<span class="cab-menu__item-ic">
								<span class="icon-user"></span>
							</span>
                    <span class="cab-menu__item-tx">@lang('nav.profile.profile')</span>
                </a>
            </div>
            <div class="cab-menu__item">
                <a href="{{ route('logout') }}">
							<span class="cab-menu__item-ic">
								<span class="icon-logout"></span>
							</span>
                    <span class="cab-menu__item-tx">@lang('nav.profile.logout')</span>
                </a>
            </div>
        </div>
    </div>
</div>
