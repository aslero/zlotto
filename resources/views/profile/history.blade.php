@extends('layouts.app')

@section('content')
    <div class="cab-page">
        <div class="wrapper cab-page__wrapper">
            @include('profile.partials.menu')
            <div class="cab-page__content">

                <h1>@lang('nav.profile.past_number')</h1>

                <div class="cab-nms">

                    <div class="cab-nms__wrap">
                        <div class="cab-nms__level">John Malkolm</div>
                        <ul class="cab-nms__list">
                            <li class="cab-nms__item">
                                <a href="">John Malkolm</a>
                            </li>
                            <li class="cab-nms__item">
                                <a href="">John Stone</a>
                            </li>
                            <li class="cab-nms__item cab-nms__item_current">
                                <a href="">John Malkolm</a>
                            </li>
                            <li class="cab-nms__item">
                                <a href="">Peter Stanbridge</a>
                            </li>
                            <li class="cab-nms__item">
                                <a href="">Trevor Virtue</a>
                            </li>
                            <li class="cab-nms__item">
                                <a href="">Eugenia Anders</a>
                            </li>
                            <li class="cab-nms__item">
                                <a href="">Verona Blair</a>
                            </li>
                        </ul>
                    </div>

                </div>

                <div class="cab-hist">

                    <div class="cab-hist__top">

                        <div class="stitle stitle_lvl_2 cab-hist__stitle">@lang('p_numbers.l_numbers') <span>John Malkolm</span></div>

                        <div class="cab-hist-f cab-hist__cab-hist-f">

                            <div class="cab-hist-f__item">
                                <div class="cab-hist-f__level">choose date</div>
                                <div class="cab-hist-f__value">
                                    <select class="select">
                                        <option selected="selected" disabled="disabled">@lang('p_numbers.date')</option>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                </div>
                            </div>

                            <div class="cab-hist-f__item">
                                <div class="cab-hist-f__level">@lang('p_numbers.choose')</div>
                                <div class="cab-hist-f__value">
                                    <select class="select">
                                        <option selected="selected" disabled="disabled">@lang('p_numbers.lottery')</option>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="cab-hist__tbl">

                        <div class="cab-hist__tbl-wrap">

                            <div class="cab-hist__tbl-head">
                                <div class="cab-hist__tbl-item_title">@lang('p_numbers.lottery')</div>
                                <div class="cab-hist__tbl-item_date">@lang('p_numbers.date')</div>
                                <div class="cab-hist__tbl-item_time">@lang('p_numbers.time')</div>
                                <div class="cab-hist__tbl-item_numbers">@lang('p_numbers.w_numbers')</div>
                            </div>

                            <div class="cab-hist__tbl-list">

                                <div class="cab-hist__tbl-list-tr">
                                    <div class="cab-hist__tbl-item_title">Lucky Day Lotto</div>
                                    <div class="cab-hist__tbl-item_date">12.04.19</div>
                                    <div class="cab-hist__tbl-item_time">06:30 PM</div>
                                    <div class="cab-hist__tbl-item_numbers">3, 33, 7, 3, 33, 7</div>
                                </div>

                                <div class="cab-hist__tbl-list-tr">
                                    <div class="cab-hist__tbl-item_title">Mega Millions With Megaplier</div>
                                    <div class="cab-hist__tbl-item_date">23.04.19</div>
                                    <div class="cab-hist__tbl-item_time">05:30 AM</div>
                                    <div class="cab-hist__tbl-item_numbers">4, 21, 9, 17, 22, 5</div>
                                </div>

                                <div class="cab-hist__tbl-list-tr">
                                    <div class="cab-hist__tbl-item_title">Extra Shot Lotto</div>
                                    <div class="cab-hist__tbl-item_date">17.05.19</div>
                                    <div class="cab-hist__tbl-item_time">02:30 PM</div>
                                    <div class="cab-hist__tbl-item_numbers">1, 54, 52, 4, 21, 9</div>
                                </div>

                                <div class="cab-hist__tbl-list-tr">
                                    <div class="cab-hist__tbl-item_title">Lucky Day Lotto</div>
                                    <div class="cab-hist__tbl-item_date">12.04.19</div>
                                    <div class="cab-hist__tbl-item_time">06:30 PM</div>
                                    <div class="cab-hist__tbl-item_numbers">3, 33, 7, 3, 33, 7</div>
                                </div>

                                <div class="cab-hist__tbl-list-tr">
                                    <div class="cab-hist__tbl-item_title">Mega Millions With Megaplier</div>
                                    <div class="cab-hist__tbl-item_date">23.04.19</div>
                                    <div class="cab-hist__tbl-item_time">05:30 AM</div>
                                    <div class="cab-hist__tbl-item_numbers">4, 21, 9, 17, 22, 5</div>
                                </div>

                                <div class="cab-hist__tbl-list-tr">
                                    <div class="cab-hist__tbl-item_title">Extra Shot Lotto</div>
                                    <div class="cab-hist__tbl-item_date">17.05.19</div>
                                    <div class="cab-hist__tbl-item_time">02:30 PM</div>
                                    <div class="cab-hist__tbl-item_numbers">1, 54, 52, 4, 21, 9</div>
                                </div>

                                <div class="cab-hist__tbl-list-tr">
                                    <div class="cab-hist__tbl-item_title">Lucky Day Lotto</div>
                                    <div class="cab-hist__tbl-item_date">12.04.19</div>
                                    <div class="cab-hist__tbl-item_time">06:30 PM</div>
                                    <div class="cab-hist__tbl-item_numbers">3, 33, 7, 3, 33, 7</div>
                                </div>

                                <div class="cab-hist__tbl-list-tr">
                                    <div class="cab-hist__tbl-item_title">Mega Millions With Megaplier</div>
                                    <div class="cab-hist__tbl-item_date">23.04.19</div>
                                    <div class="cab-hist__tbl-item_time">05:30 AM</div>
                                    <div class="cab-hist__tbl-item_numbers">4, 21, 9, 17, 22, 5</div>
                                </div>

                                <div class="cab-hist__tbl-list-tr">
                                    <div class="cab-hist__tbl-item_title">Extra Shot Lotto</div>
                                    <div class="cab-hist__tbl-item_date">17.05.19</div>
                                    <div class="cab-hist__tbl-item_time">02:30 PM</div>
                                    <div class="cab-hist__tbl-item_numbers">1, 54, 52, 4, 21, 9</div>
                                </div>

                            </div>

                        </div>

                    </div>

                </div><!-- .cab-hist -->

                <div class="cab-more">
                    <a href="" class="ic-a">@lang('p_numbers.show') 50</a>
                </div>

            </div>
        </div>
    </div>
@endsection
