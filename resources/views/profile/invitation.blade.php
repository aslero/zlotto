@extends('layouts.app')

@section('content')
    <div class="cab-page">
        <div class="wrapper cab-page__wrapper">
            @include('profile.partials.menu')
            <div class="cab-page__content">

                <h1>@lang('nav.profile.add_relative')</h1>

                <div class="invite">

                    <div class="invite__promo">

                        <div class="invite__promo-txt">
                            @lang('relative.description')
                        </div>

                        <div class="invite__promo-img">
                            <img src="i/invite-promo.svg" alt=""/>
                        </div>

                    </div>

                    <div class="invite__form">

                        <form method="POST" action="{{ route('invitations') }}">
                            @csrf
                            <div class="invite__form-item form-item">
                                <div class="form-item__level">@lang('relative.name')</div>
                                <div class="form-item__value">
                                    <input type="text" name="name" class="input form-item__input input-name @error('name') input_error @enderror" value="{{ old('name') }}" placeholder="John"/>
                                    @error('name')
                                    <div class="input-warning">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="invite__form-item form-item">
                                <div class="form-item__level">@lang('relative.email')</div>
                                <div class="form-item__value">
                                    <input type="email" name="email" class="input form-item__input input-mail @error('email') input_error @enderror" value="{{ old('email') }}" placeholder="johnmalkolm@gmail.com"/>
                                    @error('email')
                                    <div class="input-warning">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="invite__bt">
                                <button type="submit" class="btn invite__btn">@lang('relative.send')</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="invites">
                    <div class="invites__top">
                        <h3>Your invitations</h3>
                    </div>
                    <div class="invites__tbl">
                        <div class="invites__wrap">
                            <div class="invites__head">
                                <div class="invites-item_name">@lang('relative.name')</div>
                                <div class="invites-item_mail">@lang('relative.email')</div>
                                <div class="invites-item_date">@lang('relative.date')</div>
                                <div class="invites-item_status">@lang('relative.status')</div>
                            </div>

                            <div class="invites__list">
                                @forelse(Auth::user()->invitations as $invitation)
                                <div class="invites__item">
                                    <div class="invites-item_name">{{ $invitation->name }}</div>
                                    <div class="invites-item_mail">{{ $invitation->email }}</div>
                                    <div class="invites-item_date">{{ \Illuminate\Support\Carbon::parse($invitation->updated_at)->format('d.m.Y') }}</div>
                                    <div class="invites-item_status">
                                        @if($invitation->status == 1)
                                            Waiting
                                        @endif
                                        @if($invitation->status == 2)
                                            <span class="text-blue">Accepted</span>
                                        @endif
                                    </div>
                                </div>
                                @empty
                                @endforelse
                            </div>

                        </div>

                    </div>

                </div><!-- .invites -->

            </div>
        </div>
    </div>
@endsection
