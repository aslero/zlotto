@extends('layouts.app')

@section('content')
    <div class="cab-page">
        <div class="wrapper cab-page__wrapper">
            @include('profile.partials.menu')
            <div class="cab-page__content">

                <h1>Questionnaire</h1>

                <div class="pg-steps pg-steps_quest">

                    <div class="pg-steps__item pg-steps__item_active">
                        <div class="pg-steps__line"></div>
                        <div class="pg-steps__numb"><span>1</span></div>
                    </div>

                    <div class="pg-steps__item pg-steps__item_active">
                        <div class="pg-steps__line"></div>
                        <div class="pg-steps__numb"><span>2</span></div>
                    </div>

                    <div class="pg-steps__item pg-steps__item_active pg-steps__item_show">
                        <div class="pg-steps__line"></div>
                        <div class="pg-steps__numb"><span>3</span></div>
                    </div>

                </div><!-- .pg-steps -->

                <div class="quest">

                    <div class="quest__intro">
                        @lang('question.3_desc')
                    </div>

                    <form method="POST" action="{{ route('questionnaire.store') }}" class="quest__form">
                        @csrf
                        <input type="hidden" name="position" value="3">
                        @include('tpl.question')

                        <div class="quest__bt">
                            @if(Auth::user()->steps->step == 2)
                                <button type="submit" class="btn quest__btn">@lang('question.finish')</button>
                            @endif

                        </div>

                    </form>

                </div><!-- .quest -->

            </div>
        </div>
    </div>
@endsection
