@extends('layouts.app')

@section('content')
    <div class="cab-page">
        <div class="wrapper cab-page__wrapper">
            @include('profile.partials.menu')
            <div class="cab-page__content">

                <h1>Questionnaire</h1>

                <div class="pg-steps pg-steps_quest">

                    <div class="pg-steps__item pg-steps__item_active">
                        <div class="pg-steps__line"></div>
                        <div class="pg-steps__numb"><span>1</span></div>
                    </div>

                    <div class="pg-steps__item pg-steps__item_cur pg-steps__item_show">
                        <div class="pg-steps__line"></div>
                        <div class="pg-steps__numb"><span>2</span></div>
                    </div>

                    <div class="pg-steps__item">
                        <div class="pg-steps__line"></div>
                        <div class="pg-steps__numb"><span>3</span></div>
                    </div>

                </div><!-- .pg-steps -->

                <div class="quest">

                    <form method="POST" action="{{ route('questionnaire.store') }}" class="quest__form">
                        @csrf
                        <input type="hidden" name="position" value="2">
                        @include('tpl.question')

                        <div class="quest__bt">
                            @if(Auth::user()->steps->step == 1)
                                <button type="submit" class="btn quest__btn">@lang('question.next')</button>
                            @else
                                <a href="{{ route('questionnaire.step',3) }}" class="btn quest__btn">@lang('question.next')</a>
                            @endif
                        </div>

                    </form>

                </div><!-- .quest -->

            </div>
        </div>
    </div>
@endsection
