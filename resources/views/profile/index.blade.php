@extends('layouts.app')

@section('content')
    <div class="cab-page">
        <div class="wrapper cab-page__wrapper">
            @include('profile.partials.menu')
            <div class="cab-page__content">
                <h1>@lang('nav.profile.profile')</h1>
                <div class="cab-basic">
                    <div class="cab-pg">
                        <div class="cab-top">
                            <div class="stitle stitle_lvl_3">@lang('profile.billing')</div>
                            <div class="tooltip cab-top__tooltip" title="@lang('profile.billing_t')">?</div>
                        </div>
                        <div class="cab-pg__lots">
                            <div class="cab-pg__lots-item cab-pg__lots-item_active">
                                <img src="i/logo/pick.png" srcset="i/logo/pick@2x.png 2x" alt=""/>
                            </div>
                            <div class="cab-pg__lots-item">
                                <img src="i/logo/lotto.png" srcset="i/logo/lotto@2x.png 2x" alt=""/>
                            </div>
                            <div class="cab-pg__lots-item">
                                <img src="i/logo/mega.png" srcset="i/logo/mega@2x.png 2x" alt=""/>
                            </div>
                        </div>
                        <div class="cab-pg__numbers">
                            <div class="cab-pg__numbers-lvl">@lang('profile.my_numbers')</div>
                            <div class="cab-pg__numbers-val">
                                <div class="cab-pg__numbers-item">9</div>
                                <div class="cab-pg__numbers-item">33</div>
                                <div class="cab-pg__numbers-item">4</div>
                                <div class="cab-pg__numbers-item">15</div>
                            </div>
                        </div>
                        <div class="cab-pg__more">
                            <a href="" class="btn btn_border">@lang('buttons.learn_more')</a>
                        </div>
                    </div>
                    <div class="cab-members">
                        <div class="cab-top">
                            <div class="stitle stitle_lvl_3">@lang('profile.family_numbers')</div>
                            <div class="tooltip cab-top__tooltip" title="@lang('profile.family_numbers_t')">?</div>
                        </div>
                        <div class="cab-members__total">5</div>
                        <div class="cab-members__more">
                            <a href="" class="btn btn_border">@lang('buttons.learn_more')</a>
                        </div>
                    </div>
                    <div class="cab-hist cab-basic__cab-hist">
                        <div class="cab-hist__top">
                            <div class="stitle stitle_lvl_3 cab-hist__stitle">@lang('profile.pay_history') <!--<span>John Malkolm</span>--></div>
                        </div>
                        <div class="cab-hist__tbl">
                            <div class="cab-hist__tbl-wrap">
                                <div class="cab-hist__tbl-head">
                                    <div class="cab-hist__tbl-item_title">Lottery</div>
                                    <div class="cab-hist__tbl-item_date">Date</div>
                                    <div class="cab-hist__tbl-item_time">Time</div>
                                    <div class="cab-hist__tbl-item_numbers">Winning numbers</div>
                                </div>
                                <div class="cab-hist__tbl-list">
                                    <div class="cab-hist__tbl-list-tr">
                                        <div class="cab-hist__tbl-item_title">Lucky Day Lotto</div>
                                        <div class="cab-hist__tbl-item_date">12.04.19</div>
                                        <div class="cab-hist__tbl-item_time">06:30 PM</div>
                                        <div class="cab-hist__tbl-item_numbers">3, 33, 7, 3, 33, 7</div>
                                    </div>
                                    <div class="cab-hist__tbl-list-tr">
                                        <div class="cab-hist__tbl-item_title">Mega Millions With Megaplier</div>
                                        <div class="cab-hist__tbl-item_date">23.04.19</div>
                                        <div class="cab-hist__tbl-item_time">05:30 AM</div>
                                        <div class="cab-hist__tbl-item_numbers">4, 21, 9, 17, 22, 5</div>
                                    </div>
                                    <div class="cab-hist__tbl-list-tr">
                                        <div class="cab-hist__tbl-item_title">Extra Shot Lotto</div>
                                        <div class="cab-hist__tbl-item_date">17.05.19</div>
                                        <div class="cab-hist__tbl-item_time">02:30 PM</div>
                                        <div class="cab-hist__tbl-item_numbers">1, 54, 52, 4, 21, 9</div>
                                    </div>
                                    <div class="cab-hist__tbl-list-tr">
                                        <div class="cab-hist__tbl-item_title">Lucky Day Lotto</div>
                                        <div class="cab-hist__tbl-item_date">12.04.19</div>
                                        <div class="cab-hist__tbl-item_time">06:30 PM</div>
                                        <div class="cab-hist__tbl-item_numbers">3, 33, 7, 3, 33, 7</div>
                                    </div>
                                    <div class="cab-hist__tbl-list-tr">
                                        <div class="cab-hist__tbl-item_title">Mega Millions With Megaplier</div>
                                        <div class="cab-hist__tbl-item_date">23.04.19</div>
                                        <div class="cab-hist__tbl-item_time">05:30 AM</div>
                                        <div class="cab-hist__tbl-item_numbers">4, 21, 9, 17, 22, 5</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cab-hist__more">
                            <a href="" class="btn btn_border">@lang('buttons.learn_more')</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
