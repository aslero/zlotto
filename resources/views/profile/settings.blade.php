@extends('layouts.app')

@section('content')
    <div class="cab-page">
        <div class="wrapper cab-page__wrapper">
            @include('profile.partials.menu')
            <div class="cab-page__content">

                <h1>@lang('nav.profile.settings')</h1>

                <div class="cab-set">

                    <div class="cab-set__img"></div>

                    <form method="POST" action="{{ route('settings') }}">
                        @csrf
                        <div class="form-item cab-set__form-item">
                            <div class="form-item__level">@lang('settings.login')</div>
                            <div class="form-item__value">
                                <input type="text" name="name" class="input form-item__input input-fname   @error('name') input_error @enderror" value="{{ old('name') ?: Auth::user()->name }}" placeholder="Login"/>
                                @error('name')
                                <div class="input-warning">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-item cab-set__form-item">
                            <div class="form-item__level">@lang('settings.first_name')</div>
                            <div class="form-item__value">
                                <input type="text" name="first_name" class="input form-item__input input-fname   @error('first_name') input_error @enderror" value="{{ old('first_name') ?: Auth::user()->first_name }}" placeholder="John"/>
                                @error('first_name')
                                <div class="input-warning">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-item cab-set__form-item">
                            <div class="form-item__level">@lang('settings.last_name')</div>
                            <div class="form-item__value">
                                <input type="text" name="last_name" class="input form-item__input input-lname   @error('last_name') input_error @enderror" value="{{ old('last_name') ?: Auth::user()->last_name }}" placeholder="Malkolm"/>
                                @error('last_name')
                                <div class="input-warning">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-item cab-set__form-item">
                            <div class="form-item__level">@lang('settings.birth')</div>
                            <div class="form-item__value">
                                <input type="text" name="birth" class="input form-item__input input-bday  @error('birth') input_error @enderror" value="{{ old('birth') ?: Auth::user()->birth }}" placeholder="17-09-1995"/>
                                @error('birth')
                                <div class="input-warning">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-item cab-set__form-item">
                            <div class="form-item__level">@lang('settings.email')</div>
                            <div class="form-item__value">
                                <input type="email" name="email" class="input form-item__input input-mail @error('email') input_error @enderror" value="{{ old('email') ?: Auth::user()->email }}" placeholder="krutopridumal@mail.com"/>
                                @error('email')
                                <div class="input-warning">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-item cab-set__form-item">
                            <div class="form-item__level">@lang('settings.password')</div>
                            <div class="form-item__value">
                                <input type="password" name="pass" class="input form-item__input input-passw @error('pass') input_error @enderror" value="{{ old('pass') ?: Auth::user()->pass }}" placeholder="------"/>
                                @error('pass')
                                <div class="input-warning">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-item cab-set__form-item">
                            <div class="form-item__level">@lang('settings.new_password')</div>
                            <div class="form-item__value">
                                <input type="password" name="new_pass" class="input form-item__input input-passw-new" value="{{ old('new_pass')}}" placeholder="------"/>
                            </div>
                        </div>
                        <div class="form-item cab-set__form-item">
                            <div class="form-item__level">@lang('settings.confirm_password')</div>
                            <div class="form-item__value">
                                <input type="password" name="confirm_pass" class="input form-item__input input-passw-re" value="{{ old('confirm_pass') }}" placeholder="------"/>
                            </div>
                        </div>
                        <div class="form-check cab-set__form-check">
                            <label class="form-check__label">
							<span class="checkbox form-check__checkbox">
								<input type="checkbox" class="checkbox__input" name="notif_mail" @if(Auth::user()->notifications->mail == 1) checked="checked" @endif/><i class="ic-a"></i>
							</span>@lang('settings.mail_notif')
                            </label>
                        </div>
                        <div class="cab-set__bt">
                            <button type="submit" class="btn cab-set__btn">@lang('settings.save')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
