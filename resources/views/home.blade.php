@extends('layouts.app')

@section('content')
    <div class="section main-1" data-anchor="main1">
        <div class="main-1__line1"></div>
        <div class="main-1__line2"></div>

        <div class="main-1__sl">

            <div class="main-1__list">

                <div class="main-1__item">
                    <div class="wrapper">
                        <div class="main-1__item-img">
                            <div class="main-1__item-img-wrap">
                                <img src="/i/main1/1.png?2" alt=""/>
                                <div class="main-1__item-circle"></div>
                            </div>
                        </div>
                        <div class="main-1__item-entry">
                            <div class="main-1__item-title">What do you know <span class="dib">about numerology?</span></div>
                            <div class="main-1__item-text">
                                Numerology is the ancient esoteric science of numbers. It is often called the magic of numbers, in fact, this science is much closer to astrology than to magic.
                            </div>
                            <div class="main-1__item-more">
                                <a href="" class="ic-a">@lang('buttons.learn_more')</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="main-1__item">
                    <div class="wrapper main-1__item-wrapper">
                        <div class="main-1__item-img">
                            <div class="main-1__item-img-wrap">
                                <img src="/i/main1/1.png?2" alt=""/>
                                <div class="main-1__item-circle"></div>
                            </div>
                        </div>
                        <div class="main-1__item-entry">
                            <div class="main-1__item-title">What do you know <span class="dib">about numerology?</span></div>
                            <div class="main-1__item-text">
                                Numerology is the ancient esoteric science of numbers. It is often called the magic of numbers, in fact, this science is much closer to astrology than to magic.
                            </div>
                            <div class="main-1__item-more">
                                <a href="" class="main-1__item-more-lnk ic-a">@lang('buttons.learn_more')</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="main-1__arr">

                <div class="sl-arr sl-arr_r ic-a"></div>
                <div class="sl-arr sl-arr_l ic-a"></div>

            </div>

        </div>

        <div class="main-1__scroll">
            <div class="main-1__scroll-tx">@lang('buttons.scroll')</div>
        </div>
    </div>
    <div class="section main-2" data-anchor="main2">

        <div class="main-2__dots1"></div>
        <div class="main-2__dots2"></div>
        <div class="main-2__line"></div>
        <div class="main-2__img"></div>

        <div class="main-2__wrapper wrapper">

            <div class="main-2__subtitle">@lang('choose.index_num')</div>

            <div class="main-2__content">

                <div class="main-2__content-bg"></div>

                <div class="main-2__f">

                    <div class="main-2__f-level">@lang('choose.c_lottery')</div>

                    <!--
                    <div class="main-2__f-value">

                        <div class="main-2__f-item">
                            <select class="select">
                                <option selected="selected" disabled="disabled">Date of lottery</option>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                            </select>
                        </div>

                        <div class="main-2__f-item">
                            <select class="select">
                                <option selected="selected" disabled="disabled">Time of lottery</option>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                            </select>
                        </div>

                    </div>
                    -->

                </div>

                <div class="main-2__sl">

                    <div class="main-2__list">

                        <div class="main-2__item">
                            <div class="main-2__item-logo">
                                <img src="/i/logo/lottoamerica.png" srcset="i/logo/lottoamerica@2x.png 2x" alt="" />
                            </div>
                            <div class="main-2__item-next">Next Drawing: <span>Thursday, Apr 11</span></div>
                            <div class="main-2__item-line"></div>
                            <div class="main-2__item-entry">
                                <div class="main-2__item-cur">Current Estimated Jackpot</div>
                                <div class="main-2__item-jackpot">$17.85 Million</div>
                                <div class="main-2__item-cash">$11.07 Million Cash Value</div>
                            </div>
                        </div>

                        <div class="main-2__item">
                            <div class="main-2__item-logo">
                                <img src="/i/logo/power.png" srcset="i/logo/power@2x.png 2x" alt="" />
                            </div>
                            <div class="main-2__item-next">Next Drawing: <span>Thursday, Apr 11</span></div>
                            <div class="main-2__item-line"></div>
                            <div class="main-2__item-entry">
                                <div class="main-2__item-cur">Current Estimated Jackpot</div>
                                <div class="main-2__item-jackpot">$17.85 Million</div>
                                <div class="main-2__item-cash">$11.07 Million Cash Value</div>
                            </div>
                        </div>

                        <div class="main-2__item">
                            <div class="main-2__item-logo">
                                <img src="/i/logo/2by2.png" srcset="i/logo/2by2@2x.png 2x" alt="" />
                            </div>
                            <div class="main-2__item-next">Next Drawing: <span>Thursday, Apr 11</span></div>
                            <div class="main-2__item-line"></div>
                            <div class="main-2__item-entry">
                                <div class="main-2__item-cur">Current Estimated Jackpot</div>
                                <div class="main-2__item-jackpot">$17.85 Million</div>
                                <div class="main-2__item-cash">$11.07 Million Cash Value</div>
                            </div>
                        </div>

                        <div class="main-2__item">
                            <div class="main-2__item-logo">
                                <img src="/i/logo/lottoamerica.png" srcset="i/logo/lottoamerica@2x.png 2x" alt="" />
                            </div>
                            <div class="main-2__item-next">Next Drawing: <span>Thursday, Apr 11</span></div>
                            <div class="main-2__item-line"></div>
                            <div class="main-2__item-entry">
                                <div class="main-2__item-cur">Current Estimated Jackpot</div>
                                <div class="main-2__item-jackpot">$17.85 Million</div>
                                <div class="main-2__item-cash">$11.07 Million Cash Value</div>
                            </div>
                        </div>

                        <div class="main-2__item">
                            <div class="main-2__item-logo">
                                <img src="/i/logo/power.png" srcset="i/logo/power@2x.png 2x" alt="" />
                            </div>
                            <div class="main-2__item-next">Next Drawing: <span>Thursday, Apr 11</span></div>
                            <div class="main-2__item-line"></div>
                            <div class="main-2__item-entry">
                                <div class="main-2__item-cur">Current Estimated Jackpot</div>
                                <div class="main-2__item-jackpot">$17.85 Million</div>
                                <div class="main-2__item-cash">$11.07 Million Cash Value</div>
                            </div>
                        </div>

                        <div class="main-2__item">
                            <div class="main-2__item-logo">
                                <img src="/i/logo/2by2.png" srcset="i/logo/2by2@2x.png 2x" alt="" />
                            </div>
                            <div class="main-2__item-next">Next Drawing: <span>Thursday, Apr 11</span></div>
                            <div class="main-2__item-line"></div>
                            <div class="main-2__item-entry">
                                <div class="main-2__item-cur">Current Estimated Jackpot</div>
                                <div class="main-2__item-jackpot">$17.85 Million</div>
                                <div class="main-2__item-cash">$11.07 Million Cash Value</div>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="main-2__arr">

                    <div class="sl-arr sl-arr_l ic-a"></div>
                    <div class="sl-arr sl-arr_r ic-a"></div>

                </div>

            </div>

            <form class="main-2__app" action="#">

                <div class="main-2__app-form-item form-item">
                    <div class="form-item__level">@lang('settings.name')</div>
                    <div class="form-item__value">
                        <input type="text" class="input form-item__input input-name" placeholder="John"/>
                    </div>
                </div>

                <div class="main-2__app-form-item form-item">
                    <div class="form-item__level">@lang('settings.birth')</div>
                    <div class="form-item__value">
                        <input type="text" class="input form-item__input input-bday" placeholder="DD.MM.YYYY"/>
                    </div>
                </div>

                <div class="main-2__app-bt">
                    <button type="submit" class="btn main-2__app-btn">@lang('buttons.increse')</button>
                </div>

            </form>

        </div>

    </div><!-- .main-2 -->

    <div class="section main-3" data-anchor="main3">

        <div class="main-3__dots1"></div>
        <div class="main-3__dots2"></div>

        <div class="main-3__wrapper wrapper">

            <div class="main-3__sl">

                <div class="main-3__list">

                    <div class="main-3__lot-item lot-item">
                        <a href="" class="lot-item__inn">
                            <span class="lot-item__logo">
                                <img src="/i/logo/luckyday.png" srcset="i/logo/luckyday@2x.png 2x" alt=""/>
                            </span>
                            <span class="lot-item__entry">
                                <span class="lot-item__title">Lucky Day Lotto</span>
                                <span class="lot-item__price">
                                    <span class="lot-item__price-i">
                                        <span class="lot-item__price-l">Ticket Price</span>
                                        <span class="lot-item__price-v">$1</span>
                                    </span>
                                    <span class="lot-item__price-i">
                                        <span class="lot-item__price-l">Top Prize</span>
                                        <span class="lot-item__price-v">$10,000</span>
                                    </span>
                                </span>
                            </span>
                        </a>
                    </div>

                    <div class="main-3__lot-item lot-item">
                        <a href="" class="lot-item__inn">
                            <span class="lot-item__logo">
                                <img src="/i/logo/power2.png" srcset="i/logo/power2@2x.png 2x" alt=""/>
                            </span>
                            <span class="lot-item__entry">
                                <span class="lot-item__title">Pick 3 Plus Fireball</span>
                                <span class="lot-item__price">
                                    <span class="lot-item__price-i">
                                        <span class="lot-item__price-l">Ticket Price</span>
                                        <span class="lot-item__price-v">$1</span>
                                    </span>
                                    <span class="lot-item__price-i">
                                        <span class="lot-item__price-l">Top Prize</span>
                                        <span class="lot-item__price-v">$15,000</span>
                                    </span>
                                </span>
                            </span>
                        </a>
                    </div>

                    <div class="main-3__lot-item lot-item">
                        <a href="" class="lot-item__inn">
                            <span class="lot-item__logo">
                                <img src="/i/logo/mega.png" srcset="i/logo/mega@2x.png 2x" alt=""/>
                            </span>
                            <span class="lot-item__entry">
                                <span class="lot-item__title">Mega Millions With Megaplier</span>
                                <span class="lot-item__price">
                                    <span class="lot-item__price-i">
                                        <span class="lot-item__price-l">Ticket Price</span>
                                        <span class="lot-item__price-v">$1</span>
                                    </span>
                                    <span class="lot-item__price-i">
                                        <span class="lot-item__price-l">Top Prize</span>
                                        <span class="lot-item__price-v">$20,000</span>
                                    </span>
                                </span>
                            </span>
                        </a>
                    </div>

                    <div class="main-3__lot-item lot-item">
                        <a href="" class="lot-item__inn">
                            <span class="lot-item__logo">
                                <img src="/i/logo/mega.png" srcset="i/logo/mega@2x.png 2x" alt=""/>
                            </span>
                            <span class="lot-item__entry">
                                <span class="lot-item__title">Mega Millions With Megaplier</span>
                                <span class="lot-item__price">
                                    <span class="lot-item__price-i">
                                        <span class="lot-item__price-l">Ticket Price</span>
                                        <span class="lot-item__price-v">$1</span>
                                    </span>
                                    <span class="lot-item__price-i">
                                        <span class="lot-item__price-l">Top Prize</span>
                                        <span class="lot-item__price-v">$8,000</span>
                                    </span>
                                </span>
                            </span>
                        </a>
                    </div>

                    <div class="main-3__lot-item lot-item">
                        <a href="" class="lot-item__inn">
                            <span class="lot-item__logo">
                                <img src="/i/logo/pick.png" srcset="i/logo/pick@2x.png 2x" alt=""/>
                            </span>
                            <span class="lot-item__entry">
                                <span class="lot-item__title">Pick 3 Plus Fireball</span>
                                <span class="lot-item__price">
                                    <span class="lot-item__price-i">
                                        <span class="lot-item__price-l">Ticket Price</span>
                                        <span class="lot-item__price-v">$1</span>
                                    </span>
                                    <span class="lot-item__price-i">
                                        <span class="lot-item__price-l">Top Prize</span>
                                        <span class="lot-item__price-v">$7,000</span>
                                    </span>
                                </span>
                            </span>
                        </a>
                    </div>

                    <div class="main-3__lot-item lot-item">
                        <a href="" class="lot-item__inn">
                            <span class="lot-item__logo">
                                <img src="/i/logo/lotto.png" srcset="i/logo/lotto@2x.png 2x" alt=""/>
                            </span>
                            <span class="lot-item__entry">
                                <span class="lot-item__title">Lucky Day Lotto</span>
                                <span class="lot-item__price">
                                    <span class="lot-item__price-i">
                                        <span class="lot-item__price-l">Ticket Price</span>
                                        <span class="lot-item__price-v">$1</span>
                                    </span>
                                    <span class="lot-item__price-i">
                                        <span class="lot-item__price-l">Top Prize</span>
                                        <span class="lot-item__price-v">$5,000</span>
                                    </span>
                                </span>
                            </span>
                        </a>
                    </div>

                    <div class="main-3__lot-item lot-item">
                        <a href="" class="lot-item__inn">
                            <span class="lot-item__logo">
                                <img src="/i/logo/lotto.png" srcset="i/logo/lotto@2x.png 2x" alt=""/>
                            </span>
                            <span class="lot-item__entry">
                                <span class="lot-item__title">Extra Shot Lotto</span>
                                <span class="lot-item__price">
                                    <span class="lot-item__price-i">
                                        <span class="lot-item__price-l">Ticket Price</span>
                                        <span class="lot-item__price-v">$1</span>
                                    </span>
                                    <span class="lot-item__price-i">
                                        <span class="lot-item__price-l">Top Prize</span>
                                        <span class="lot-item__price-v">$5,000</span>
                                    </span>
                                </span>
                            </span>
                        </a>
                    </div>

                    <div class="main-3__lot-item lot-item">
                        <a href="" class="lot-item__inn">
                            <span class="lot-item__logo">
                                <img src="/i/logo/luckyday.png" srcset="i/logo/luckyday@2x.png 2x" alt=""/>
                            </span>
                            <span class="lot-item__entry">
                                <span class="lot-item__title">Extra Shot Lotto</span>
                                <span class="lot-item__price">
                                    <span class="lot-item__price-i">
                                        <span class="lot-item__price-l">Ticket Price</span>
                                        <span class="lot-item__price-v">$1</span>
                                    </span>
                                    <span class="lot-item__price-i">
                                        <span class="lot-item__price-l">Top Prize</span>
                                        <span class="lot-item__price-v">$10,000</span>
                                    </span>
                                </span>
                            </span>
                        </a>
                    </div>

                    <div class="main-3__lot-item lot-item">
                        <a href="" class="lot-item__inn">
                            <span class="lot-item__logo">
                                <img src="/i/logo/power2.png" srcset="i/logo/power2@2x.png 2x" alt=""/>
                            </span>
                            <span class="lot-item__entry">
                                <span class="lot-item__title">Pick 3 Plus Fireball</span>
                                <span class="lot-item__price">
                                    <span class="lot-item__price-i">
                                        <span class="lot-item__price-l">Ticket Price</span>
                                        <span class="lot-item__price-v">$1</span>
                                    </span>
                                    <span class="lot-item__price-i">
                                        <span class="lot-item__price-l">Top Prize</span>
                                        <span class="lot-item__price-v">$15,000</span>
                                    </span>
                                </span>
                            </span>
                        </a>
                    </div>

                    <div class="main-3__lot-item lot-item">
                        <a href="" class="lot-item__inn">
                            <span class="lot-item__logo">
                                <img src="/i/logo/power2.png" srcset="i/logo/power2@2x.png 2x" alt=""/>
                            </span>
                            <span class="lot-item__entry">
                                <span class="lot-item__title">Pick 3 Plus Fireball</span>
                                <span class="lot-item__price">
                                    <span class="lot-item__price-i">
                                        <span class="lot-item__price-l">Ticket Price</span>
                                        <span class="lot-item__price-v">$1</span>
                                    </span>
                                    <span class="lot-item__price-i">
                                        <span class="lot-item__price-l">Top Prize</span>
                                        <span class="lot-item__price-v">$15,000</span>
                                    </span>
                                </span>
                            </span>
                        </a>
                    </div>

                    <div class="main-3__lot-item lot-item">
                        <a href="" class="lot-item__inn">
                            <span class="lot-item__logo">
                                <img src="/i/logo/mega.png" srcset="i/logo/mega@2x.png 2x" alt=""/>
                            </span>
                            <span class="lot-item__entry">
                                <span class="lot-item__title">Mega Millions With Megaplier</span>
                                <span class="lot-item__price">
                                    <span class="lot-item__price-i">
                                        <span class="lot-item__price-l">Ticket Price</span>
                                        <span class="lot-item__price-v">$1</span>
                                    </span>
                                    <span class="lot-item__price-i">
                                        <span class="lot-item__price-l">Top Prize</span>
                                        <span class="lot-item__price-v">$20,000</span>
                                    </span>
                                </span>
                            </span>
                        </a>
                    </div>

                    <div class="main-3__lot-item lot-item">
                        <a href="" class="lot-item__inn">
                            <span class="lot-item__logo">
                                <img src="/i/logo/pick.png" srcset="i/logo/pick@2x.png 2x" alt=""/>
                            </span>
                            <span class="lot-item__entry">
                                <span class="lot-item__title">Pick 3 Plus Fireball</span>
                                <span class="lot-item__price">
                                    <span class="lot-item__price-i">
                                        <span class="lot-item__price-l">Ticket Price</span>
                                        <span class="lot-item__price-v">$1</span>
                                    </span>
                                    <span class="lot-item__price-i">
                                        <span class="lot-item__price-l">Top Prize</span>
                                        <span class="lot-item__price-v">$7,000</span>
                                    </span>
                                </span>
                            </span>
                        </a>
                    </div>

                    <div class="main-3__lot-item lot-item">
                        <a href="" class="lot-item__inn">
                            <span class="lot-item__logo">
                                <img src="/i/logo/luckyday.png" srcset="i/logo/luckyday@2x.png 2x" alt=""/>
                            </span>
                            <span class="lot-item__entry">
                                <span class="lot-item__title">Lucky Day Lotto</span>
                                <span class="lot-item__price">
                                    <span class="lot-item__price-i">
                                        <span class="lot-item__price-l">Ticket Price</span>
                                        <span class="lot-item__price-v">$1</span>
                                    </span>
                                    <span class="lot-item__price-i">
                                        <span class="lot-item__price-l">Top Prize</span>
                                        <span class="lot-item__price-v">$10,000</span>
                                    </span>
                                </span>
                            </span>
                        </a>
                    </div>

                    <div class="main-3__lot-item lot-item">
                        <a href="" class="lot-item__inn">
                            <span class="lot-item__logo">
                                <img src="/i/logo/power2.png" srcset="i/logo/power2@2x.png 2x" alt=""/>
                            </span>
                            <span class="lot-item__entry">
                                <span class="lot-item__title">Pick 3 Plus Fireball</span>
                                <span class="lot-item__price">
                                    <span class="lot-item__price-i">
                                        <span class="lot-item__price-l">Ticket Price</span>
                                        <span class="lot-item__price-v">$1</span>
                                    </span>
                                    <span class="lot-item__price-i">
                                        <span class="lot-item__price-l">Top Prize</span>
                                        <span class="lot-item__price-v">$15,000</span>
                                    </span>
                                </span>
                            </span>
                        </a>
                    </div>

                    <div class="main-3__lot-item lot-item">
                        <a href="" class="lot-item__inn">
                            <span class="lot-item__logo">
                                <img src="/i/logo/mega.png" srcset="i/logo/mega@2x.png 2x" alt=""/>
                            </span>
                            <span class="lot-item__entry">
                                <span class="lot-item__title">Mega Millions With Megaplier</span>
                                <span class="lot-item__price">
                                    <span class="lot-item__price-i">
                                        <span class="lot-item__price-l">Ticket Price</span>
                                        <span class="lot-item__price-v">$1</span>
                                    </span>
                                    <span class="lot-item__price-i">
                                        <span class="lot-item__price-l">Top Prize</span>
                                        <span class="lot-item__price-v">$20,000</span>
                                    </span>
                                </span>
                            </span>
                        </a>
                    </div>

                    <div class="main-3__lot-item lot-item">
                        <a href="" class="lot-item__inn">
                            <span class="lot-item__logo">
                                <img src="/i/logo/mega.png" srcset="i/logo/mega@2x.png 2x" alt=""/>
                            </span>
                            <span class="lot-item__entry">
                                <span class="lot-item__title">Mega Millions With Megaplier</span>
                                <span class="lot-item__price">
                                    <span class="lot-item__price-i">
                                        <span class="lot-item__price-l">Ticket Price</span>
                                        <span class="lot-item__price-v">$1</span>
                                    </span>
                                    <span class="lot-item__price-i">
                                        <span class="lot-item__price-l">Top Prize</span>
                                        <span class="lot-item__price-v">$8,000</span>
                                    </span>
                                </span>
                            </span>
                        </a>
                    </div>

                    <div class="main-3__lot-item lot-item">
                        <a href="" class="lot-item__inn">
                            <span class="lot-item__logo">
                                <img src="/i/logo/pick.png" srcset="i/logo/pick@2x.png 2x" alt=""/>
                            </span>
                            <span class="lot-item__entry">
                                <span class="lot-item__title">Pick 3 Plus Fireball</span>
                                <span class="lot-item__price">
                                    <span class="lot-item__price-i">
                                        <span class="lot-item__price-l">Ticket Price</span>
                                        <span class="lot-item__price-v">$1</span>
                                    </span>
                                    <span class="lot-item__price-i">
                                        <span class="lot-item__price-l">Top Prize</span>
                                        <span class="lot-item__price-v">$7,000</span>
                                    </span>
                                </span>
                            </span>
                        </a>
                    </div>

                    <div class="main-3__lot-item lot-item">
                        <a href="" class="lot-item__inn">
                            <span class="lot-item__logo">
                                <img src="/i/logo/lotto.png" srcset="i/logo/lotto@2x.png 2x" alt=""/>
                            </span>
                            <span class="lot-item__entry">
                                <span class="lot-item__title">Lucky Day Lotto</span>
                                <span class="lot-item__price">
                                    <span class="lot-item__price-i">
                                        <span class="lot-item__price-l">Ticket Price</span>
                                        <span class="lot-item__price-v">$1</span>
                                    </span>
                                    <span class="lot-item__price-i">
                                        <span class="lot-item__price-l">Top Prize</span>
                                        <span class="lot-item__price-v">$5,000</span>
                                    </span>
                                </span>
                            </span>
                        </a>
                    </div>

                    <div class="main-3__lot-item lot-item">
                        <a href="" class="lot-item__inn">
                            <span class="lot-item__logo">
                                <img src="/i/logo/lotto.png" srcset="i/logo/lotto@2x.png 2x" alt=""/>
                            </span>
                            <span class="lot-item__entry">
                                <span class="lot-item__title">Extra Shot Lotto</span>
                                <span class="lot-item__price">
                                    <span class="lot-item__price-i">
                                        <span class="lot-item__price-l">Ticket Price</span>
                                        <span class="lot-item__price-v">$1</span>
                                    </span>
                                    <span class="lot-item__price-i">
                                        <span class="lot-item__price-l">Top Prize</span>
                                        <span class="lot-item__price-v">$5,000</span>
                                    </span>
                                </span>
                            </span>
                        </a>
                    </div>

                    <div class="main-3__lot-item lot-item">
                        <a href="" class="lot-item__inn">
                            <span class="lot-item__logo">
                                <img src="/i/logo/luckyday.png" srcset="i/logo/luckyday@2x.png 2x" alt=""/>
                            </span>
                            <span class="lot-item__entry">
                                <span class="lot-item__title">Extra Shot Lotto</span>
                                <span class="lot-item__price">
                                    <span class="lot-item__price-i">
                                        <span class="lot-item__price-l">Ticket Price</span>
                                        <span class="lot-item__price-v">$1</span>
                                    </span>
                                    <span class="lot-item__price-i">
                                        <span class="lot-item__price-l">Top Prize</span>
                                        <span class="lot-item__price-v">$10,000</span>
                                    </span>
                                </span>
                            </span>
                        </a>
                    </div>

                    <div class="main-3__lot-item lot-item">
                        <a href="" class="lot-item__inn">
                            <span class="lot-item__logo">
                                <img src="/i/logo/power2.png" srcset="i/logo/power2@2x.png 2x" alt=""/>
                            </span>
                            <span class="lot-item__entry">
                                <span class="lot-item__title">Pick 3 Plus Fireball</span>
                                <span class="lot-item__price">
                                    <span class="lot-item__price-i">
                                        <span class="lot-item__price-l">Ticket Price</span>
                                        <span class="lot-item__price-v">$1</span>
                                    </span>
                                    <span class="lot-item__price-i">
                                        <span class="lot-item__price-l">Top Prize</span>
                                        <span class="lot-item__price-v">$15,000</span>
                                    </span>
                                </span>
                            </span>
                        </a>
                    </div>

                    <div class="main-3__lot-item lot-item">
                        <a href="" class="lot-item__inn">
                            <span class="lot-item__logo">
                                <img src="/i/logo/power2.png" srcset="i/logo/power2@2x.png 2x" alt=""/>
                            </span>
                            <span class="lot-item__entry">
                                <span class="lot-item__title">Pick 3 Plus Fireball</span>
                                <span class="lot-item__price">
                                    <span class="lot-item__price-i">
                                        <span class="lot-item__price-l">Ticket Price</span>
                                        <span class="lot-item__price-v">$1</span>
                                    </span>
                                    <span class="lot-item__price-i">
                                        <span class="lot-item__price-l">Top Prize</span>
                                        <span class="lot-item__price-v">$15,000</span>
                                    </span>
                                </span>
                            </span>
                        </a>
                    </div>

                    <div class="main-3__lot-item lot-item">
                        <a href="" class="lot-item__inn">
                            <span class="lot-item__logo">
                                <img src="/i/logo/mega.png" srcset="i/logo/mega@2x.png 2x" alt=""/>
                            </span>
                            <span class="lot-item__entry">
                                <span class="lot-item__title">Mega Millions With Megaplier</span>
                                <span class="lot-item__price">
                                    <span class="lot-item__price-i">
                                        <span class="lot-item__price-l">Ticket Price</span>
                                        <span class="lot-item__price-v">$1</span>
                                    </span>
                                    <span class="lot-item__price-i">
                                        <span class="lot-item__price-l">Top Prize</span>
                                        <span class="lot-item__price-v">$20,000</span>
                                    </span>
                                </span>
                            </span>
                        </a>
                    </div>

                    <div class="main-3__lot-item lot-item">
                        <a href="" class="lot-item__inn">
                            <span class="lot-item__logo">
                                <img src="/i/logo/pick.png" srcset="i/logo/pick@2x.png 2x" alt=""/>
                            </span>
                            <span class="lot-item__entry">
                                <span class="lot-item__title">Pick 3 Plus Fireball</span>
                                <span class="lot-item__price">
                                    <span class="lot-item__price-i">
                                        <span class="lot-item__price-l">Ticket Price</span>
                                        <span class="lot-item__price-v">$1</span>
                                    </span>
                                    <span class="lot-item__price-i">
                                        <span class="lot-item__price-l">Top Prize</span>
                                        <span class="lot-item__price-v">$7,000</span>
                                    </span>
                                </span>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="main-3__bot">
                <div class="main-3__arr">
                    <div class="sl-arr sl-arr_l ic-a"></div>
                    <div class="sl-arr sl-arr_r ic-a"></div>

                </div>
                <div class="main-3__pbar">
                    <div class="main-3__pbar-inn"></div>
                </div>
                <div class="main-3__num"><span class="main-3__num-cur">1</span>/<span class="main-3__num-total">5</span></div>
            </div>
        </div>
    </div>
    <div class="section main-4 fp-auto-height" data-anchor="main4">
        <div class="wrapper">
            <div class="main-4__q">
                <img src="/i/main4-q.svg" alt=""/>
            </div>
            <div class="main-4__bt">
                <a href="#modal-how" class="modalbox">@lang('buttons.how_it_works')</a>
            </div>
        </div>
    </div>
    </div>
    <div class="modal-how" id="modal-how">
        <div class="modal-how__text">
            <strong>Numerology</strong> is the ancient esoteric science of numbers. It is often called the magic of numbers, in fact, this science is much closer to astrology than to magic.
            <br/>
            <br/>
            The following principle makes the basis of numerology: all multi-digit numbers can be reduced to a single digit (simple numbers from 1 to 9), which correspond to certain occult characteristics that affect a person's life. That is, for every single number, certain properties, concepts and images are assigned. Since the letters of the alphabet can have a numeric expression through its sequence number, any words or names undergo the same numerological operations as numbers.
            <br/>
            <br/>
            Numerology, like astrology, helps to determine the temper, natural talents, strengths, and weaknesses, to predict the future, to open the most appropriate time for decision-making and for action. With the help of numerology, you can choose partners for business or marriage.
            <br/>
            <br/>
            It is difficult to say exactly where numerology originated from, for the reason that in antiquity (in Babylon, India, Egypt, Greece, and Rome), there didn't exist a separate science called numerology: people used another more common notion - arithmomancy (prediction by numbers).
            <br/>
            <br/>
            The basic principles of the current version of Western numerology were developed in the 6th century BC. Ancient Greek philosopher and mathematician Pythagoras, who combined the mathematical systems of Arabs, Druids, Phoenicians, and Egyptians with the sciences of human nature. Pythagoras traveled extensively in Egypt, Chaldea, and other countries; returning, he founded a special philosophical society in southern Italy. In this society, the Pythagorean school, studied science, especially arithmetic, geometry, and astronomy, and were made important discoveries. Pythagoras discovered that the four well-known musical intervals can be expressed in proportion between the numbers 1 to 4. Then he realized that the numbers 1 to 4, and the added numbers to them, totaling 10, form a sacred number that represents the material and the metaphysical integrity of the universe, perfection. If four notes can be expressed in numbers, Pythagoras considered, then everything can also be expressed in numbers. The universe is expressed in numbers from one to four, which, when added to the ten, become the source of all existence.
            <br/>
            <br/>
            The Pythagorean doctrine significantly influenced the formation and development of the spiritual secret societies of Europe, such as the Rosicrucians, Masons, anthroposophists and others. Since then and until now, this version of numerology continues to evolve.
            <br/>
            <br/>
            Numerology has received particular importance in the teachings of Kabbalah, where its variety is known as gematria. Kabbalists expanded the concept of Pythagoras using numbers in magic squares for various purposes. With the discovery in the 19th century by scientists of the nature of light, electricity, and magnetism, ancient occult meanings attributed to numbers were also attributed to vibrations of energy.
        </div>
        <a href="" class="modal-close" rel="modal:close"><span class="icon-x"></span></a>
    </div>
@endsection
