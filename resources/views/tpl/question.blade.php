@forelse($questions as $question)
    <div class="quest__item">
        <div class="quest__item-lvl">{{ $question->text }}</div>
        <div class="quest__item-val">
            @switch($question->type)
                @case('text')
                <input type="text" name="step[{{$question->id}}]" @if($question->required) required @endif class="input quest__input" value="{{$question->answer}}"/>
                @break
                @case('textarea')
                <textarea step[{{$question->id}}] cols="30" rows="10" @if($question->required) required @endif  class="input quest__input">{{$question->answer}}</textarea>
                @break$question->answer
                @case('date')
                <input type="text" name="step[{{$question->id}}]" @if($question->required) required @endif class="input quest__input input-bday" value="{{$question->answer}}"/>
                @break
                @case('select')
                <select name="step[{{$question->id}}]" @if($question->required) required @endif>
                    @forelse($question->options as $option)
                        <option value="{{$option->id}}" @if($option->id == $question->answer) selected @endif>{{$option->option}}</option>
                        @empty
                    @endforelse
                </select>
                @break
                @default
                <input type="text" name="step[{{$question->id}}]" @if($question->required) required @endif class="input quest__input" value="{{$question->answer}}"/>
            @endswitch
        </div>
    </div>
@empty
@endforelse
