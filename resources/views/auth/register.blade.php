@extends('layouts.app')

@section('content')
<div class="cab-start cab-start_register">
    <div class="cab-start__wrapper wrapper">
        <div class="cab-start__content">
            <div class="cab-start__subtitle">Join the Zlotto Community Today!</div>
            <div class="cab-start__inline">
                <div class="cab-start__basic">
                    <div class="cab-start__form">
                        <form method="POST" action="{{ route('register') }}" id="register">
                            @csrf
                            <div class="cab-start__form-item form-item">
                                <div class="form-item__level">YOUR FULL NAME</div>
                                <div class="form-item__value">
                                    <input type="text" class="input form-item__input input-name  @error('name') input_error @enderror" placeholder="John"  name="name" value="{{ old('name') }}" autocomplete="name" autofocus/>
                                    <div class="input-warning">Incorrect name</div>
                                </div>
                            </div>
                            <div class="cab-start__form-item form-item">
                                <div class="form-item__level">Email</div>
                                <div class="form-item__value">
                                    <input type="text" class="input form-item__input input-mail  @error('email') input_error @enderror"  name="email" value="{{ old('email') }}" autocomplete="email"/>
                                    <div class="input-warning">Incorrect email</div>
                                </div>
                            </div>
                            <div class="cab-start__form-item form-item">
                                <div class="form-item__level">Password</div>
                                <div class="form-item__value">
                                    <input type="password" class="input form-item__input input-passw  @error('password') input_error @enderror" placeholder="------"  name="password" autocomplete="new-password"/>
                                    <div class="input-warning">Incorrect password</div>
                                </div>
                            </div>
                            <div class="cab-start__bt">
                                <button type="submit" class="btn cab-start__btn">Continue</button>
                            </div>
                        </form>
                    </div>
                    <div class="cab-start__links">
                        <a href="{{ route('login') }}">Sign In</a>
                    </div>
                </div>
                <div class="cab-start__side">
                    <div class="cab-start__or">or</div>
                    <div class="cab-start__social">
                        <a href="/login/facebook" class="btn-social btn-social_fb ic-b">Sign in with Facebook</a>
                        <a href="/login/twitter" class="btn-social btn-social_tw ic-b">Sign in with Twitter</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
