@extends('layouts.app')

@section('content')
<div class="cab-start cab-start_login">
    <div class="cab-start__wrapper wrapper">
        <div class="cab-start__content">
            <div class="cab-start__subtitle">Login</div>
            <div class="cab-start__form">
                <form  method="POST" action="{{ route('login') }}" id="login">
                    @csrf
                    <div class="cab-start__form-item form-item">
                        <div class="form-item__level">USERNAME</div>
                        <div class="form-item__value">
                            <input type="text" class="input form-item__input input-name  {{ $errors->has('name') || $errors->has('email') ? ' input_error' : '' }}" placeholder="John"  name="login" value="{{ old('name') ?: old('email') }}" autocomplete="login" autofocus/>
                            <div class="input-warning">Incorrect name</div>
                        </div>
                    </div>
                    <div class="cab-start__form-item form-item">
                        <div class="form-item__level">Password</div>
                        <div class="form-item__value">
                            <input type="password" class="input form-item__input input-passw  @error('password') input_error @enderror" placeholder="------"  name="password" autocomplete="current-password"/>
                            <div class="input-warning">Incorrect password</div>
                        </div>
                    </div>
                    <div class="cab-start__form-check form-check">
                        <label class="form-check__label">
                        <span class="checkbox form-check__checkbox">
                            <input type="checkbox" class="checkbox__input pp" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}/><i class="ic-a"></i>
                        </span>Remember Me?
                        </label>
                    </div>
                    <div class="cab-start__bt">
                        <button type="submit" class="btn cab-start__btn">Continue</button>
                    </div>
                </form>
            </div>
            <div class="cab-start__links">
                @if (Route::has('password.request'))
                    <a href="{{ route('password.request') }}">
                        {{ __('Forgot Your Password?') }}
                    </a>
                @endif
                <a href="{{ route('register') }}">Join now</a>
            </div>
        </div>
    </div>
</div>
@endsection
