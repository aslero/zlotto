@extends('layouts.app')

@section('content')
    <div class="cab-start cab-start_reset">
        <div class="cab-start__wrapper wrapper">
            <div class="cab-start__content">
                <div class="cab-start__subtitle">Please enter new password.</div>
                <div class="cab-start__form">
                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf
                        <input type="hidden" name="token" value="{{ $token }}">
                        <div class="cab-start__form-item form-item">
                            <div class="form-item__level">EMail</div>
                            <div class="form-item__value">
                                <input  type="email" name="email" class="input form-item__input input-mail  @error('email') input_error @enderror"  value="{{ $email ?? old('email') }}" autocomplete="email" autofocus placeholder="example@gmail.com"/>
                                @error('email')
                                <div class="input-warning">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="cab-start__form-item form-item">
                            <div class="form-item__value">
                                <input id="password" type="password" class="input form-item__input input-passw @error('password') input_error @enderror" name="password" autocomplete="new-password"  placeholder="********">
                                @error('password')
                                <div class="input-warning">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="cab-start__form-item form-item">
                            <div class="form-item__value">
                                <input id="password-confirm" type="password" class="input form-item__input input-passw" name="password_confirmation" autocomplete="new-password"  placeholder="********">
                            </div>
                        </div>
                        <div class="cab-start__bt">
                            <button type="submit" class="btn cab-start__btn">Submit</button>
                        </div>
                    </form>
                </div>
                <div class="cab-start__links">
                    <a href="{{ route('login') }}">Back to Sign In</a>
                </div>
            </div>
        </div>
    </div>
@endsection
