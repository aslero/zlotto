@extends('layouts.app')

@section('content')
    <div class="cab-start cab-start_reset">
        <div class="cab-start__wrapper wrapper">
            <div class="cab-start__content">
                <div class="cab-start__subtitle">Please enter your email address in order to receive your password reset instructions.</div>
                <div class="cab-start__form">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                        <form method="POST" action="{{ route('password.email') }}">
                            @csrf
                        <div class="cab-start__form-item form-item">
                            <div class="form-item__level">EMail</div>
                            <div class="form-item__value">
                                <input type="email" name="email" class="input form-item__input input-mail  @error('email') input_error @enderror" placeholder="example@gmail.com"/>
                                @error('email')
                                <div class="input-warning">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="cab-start__bt">
                            <button type="submit" class="btn cab-start__btn">Submit</button>
                        </div>
                    </form>
                </div>
                <div class="cab-start__links">
                    <a href="{{ route('login') }}">Back to Sign In</a>
                </div>
            </div>
        </div>
    </div>
@endsection
