
$(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('input,select,textarea').on('change invalid', function() {
        var textfield = $(this).get(0);

        // 'setCustomValidity not only sets the message, but also marks
        // the field as invalid. In order to see whether the field really is
        // invalid, we have to remove the message first
        textfield.setCustomValidity('');

        if (!textfield.validity.valid) {
            textfield.setCustomValidity('Fill in this field!');
        }
    });

    var p_mail = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;

    $('#login').on('submit', function(e){
        e.preventDefault();
        var err = false;
        var $form = $(this);
        if($form.find('.input-name').val().length < 3)
        {
            if (!$form.find('.input-name').is('.input_error'))
            {
                $form.find('.input-name').keyup(function(){
                    if($(this).val().length < 3)
                    {
                        $(this).addClass('input_error');
                    }
                    else
                    {
                        $(this).removeClass('input_error');
                    }
                });
            }
            $form.find('.input-name').addClass('input_error');
            err = true;
        }
        else
        {
            $form.find('.input-name').removeClass('input_error');
        }
        if($form.find('.input-passw').val().length < 3)
        {
            if (!$form.find('.input-passw').is('.input_error'))
            {
                $form.find('.input-passw').keyup(function(){
                    if($(this).val().length < 3)
                    {
                        $(this).addClass('input_error');
                    }
                    else
                    {
                        $(this).removeClass('input_error');
                    }
                });
            }
            $form.find('.input-passw').addClass('input_error');
            err = true;
        }
        else
        {
            $form.find('.input-passw').removeClass('input_error');
        }
        if (!err)
        {
            $.ajax({
                method: 'POST',
                data: $form.serialize(),
                url: $form.attr('action'),
                success: function (response) {
                    if (response.error === 0){
                        toast.fire({
                            icon: 'success',
                            title: response.message
                        });
                        window.location.href = response.redirect;
                    }
                    if (response.error === 1){
                        toast.fire({
                            icon: 'error',
                            title: response.message
                        });
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toast.fire({
                        icon: 'error',
                        title: jqXHR.responseJSON.message
                    });
                    // TODO jqXHR.responseJSON может не быть
                }
            });
        }
        return false;
    });

    $('#register').on('submit', function(e){
        e.preventDefault();
        var err = false;
        var $form = $(this);
        if($form.find('.input-name').val().length < 3)
        {
            if (!$form.find('.input-name').is('.input_error'))
            {
                $form.find('.input-name').keyup(function(){
                    if($(this).val().length < 3)
                    {
                        $(this).addClass('input_error');
                    }
                    else
                    {
                        $(this).removeClass('input_error');
                    }
                });
            }
            $form.find('.input-name').addClass('input_error');
            err = true;
        }
        else
        {
            $form.find('.input-name').removeClass('input_error');
        }
        if(!p_mail.test($form.find('.input-mail').val()))
        {
            if (!$form.find('.input-mail').is('.input_error'))
            {
                $form.find('.input-mail').keyup(function(){
                    if(!p_mail.test($(this).val()))
                    {
                        $(this).addClass('input_error');
                    }
                    else
                    {
                        $(this).removeClass('input_error');
                    }
                });
            }
            $form.find('.input-mail').addClass('input_error');
            err = true;
        }
        else
        {
            $form.find('.input-mail').removeClass('input_error');
        }
        if($form.find('.input-passw').val().length < 3)
        {
            if (!$form.find('.input-passw').is('.input_error'))
            {
                $form.find('.input-passw').keyup(function(){
                    if($(this).val().length < 3)
                    {
                        $(this).addClass('input_error');
                    }
                    else
                    {
                        $(this).removeClass('input_error');
                    }
                });
            }
            $form.find('.input-passw').addClass('input_error');
            err = true;
        }
        else
        {
            $form.find('.input-passw').removeClass('input_error');
        }
        if (!err)
        {
            $.ajax({
                method: 'POST',
                data: $form.serialize(),
                url: $form.attr('action'),
                success: function (response) {
                    console.log(response);
                    if (response.error === 0){
                        toast.fire({
                            icon: 'success',
                            title: response.message
                        });
                        window.location.href = response.redirect;
                    }
                    if (response.error === 1){
                        toast.fire({
                            icon: 'error',
                            title: response.message
                        });
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toast.fire({
                        icon: 'error',
                        title: jqXHR.responseJSON.message
                    });
                    // TODO jqXHR.responseJSON может не быть
                }
            });
        }
        return false;
    });
});
