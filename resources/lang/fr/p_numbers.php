<?php

return [
    'lottery' => 'Loterie',
    'date' => 'Date',
    'time' => 'Time',
    'w_numbers' => 'Numéros gagnants',
    'choose' => 'Choisissez la loterie',
    'l_numbers' => 'Derniers numéros porte-bonheur générés pour',
    'show' => 'Spectacle',
];
