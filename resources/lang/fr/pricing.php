<?php

return [
    'title' => 'Paiement',
    'discount' => 'remise',
    'p_year' => 'Par an',
    'month' => 'mois',
    'total' => 'Totale',
    'cvv' => 'cvv',
    'error_cvv' => 'Incorrect cvv',
    'expiration_date' => 'date d\'expiration',
    'error_expiration_date' => 'Date d\'expiration incorrecte',
    'card_number' => 'Numéro de carte',
    'error_card_number' => 'Numéro de carte incorrect',
    'tariff' => 'Tarif',
    'members' => 'Members',
    'add_f_m' => 'Ajouter un membre de la famille',
    'add_f_d' => 'Vous voulez jouer avec votre famille? Donnez-leur une chance de générer leurs propres numéros porte-bonheur. Prenez ce package comme un complément pour 1,2,3 ou 4 membres!',
    'unlimited' => 'Jouez illimité pendant un mois. (1 MOIS)',
    'm_package' => 'NOTRE FORFAIT MENSUEL',
    'description' => 'Notre forfait d\'adhésion annuel le plus populaire de la communauté Zlotto. Générez des numéros porte-bonheur illimités pour une ANNÉE COMPLÈTE! (12 MOIS)',

];
