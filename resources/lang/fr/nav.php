<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'nav' => [
        'index' => 'Principale',
        'choose_lottery' => 'CHOISISSEZ LA LOTERIE',
        'about' => 'COMMENT ÇA FONCTIONNE',
        'price' => 'PRIX',
    ],
    'profile' => [
        'question' => 'Mes numéros chanceux',
        'add_relative' => 'Ajouter un parent',
        'past_number' => 'Nombres passés',
        'settings' => 'Réglages',
        'profile' => 'Mon compte',
        'logout' => 'Se déconnecter',
    ],
];
