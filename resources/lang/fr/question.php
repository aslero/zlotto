<?php

return [
    'next'   => 'Prochain',
    'finish'   => 'Terminer',
    '3_desc' => "Allumez la calculatrice, s'il vous plaît. Vous devez additionner les nombres jusqu'à ce que vous obteniez un nombre de 0 à 9. <br>Exemple: 1234567 - 1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 = 36; 3 + 6 = 9 - Utilisez le chiffre 9."
];
