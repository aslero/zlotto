<?php

return [
    'learn_more' => 'Apprendre encore plus',
    'create_account' => 'Créer un compte',
    'terms' => 'Termes et conditions',
    'byu_now' => 'ACHETER MAINTENANT',
    'how_it_works' => 'Comment ça fonctionne',
    'increse' => 'Augmentez mes chances maintenant',
    'scroll' => 'Faites défiler vers le bas pour en savoir plus',
];
