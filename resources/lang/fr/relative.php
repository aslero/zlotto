<?php

return [
    'name' => 'Nom',
    'email' => 'Email',
    'date' => 'Date',
    'status' => 'Statut',
    'send' => 'Envoyer une invitation',
    'my_relative' => 'Vos invitations',
    'description' => 'Vérifiez si votre parent a plus de chance de gagner que vous. Voulez-vous ajouter un parent? -> Vous voulez jouer avec votre famille ou vos proches? Voyez s\'ils ont plus de chance que vous! Ajoutez un parent ici.',
];
