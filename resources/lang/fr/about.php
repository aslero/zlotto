<?php

return [
    'step_1' => 'Créez un <br/> <u> compte Zlotto </u>',
    'step_2' => 'Répondez à un questionnaire personnalisé adapté à vous et aux événements importants de votre <u> vie </u>',
    'step_3' => '<u> Payez une petite cotisation </u> (cela aide Zlotto à rester en affaires!)',
    'step_4' => '<u> Choisissez votre marque de loterie </u> préférée, la date, l\'heure et combien vous allez jouer',
    'step_5' => 'Nos experts en numérologie <u> calculeront </u> votre ensemble unique de <u> nombres </u> <span> à jouer en fonction de votre vie! </span>',
    'step_6' => '<u> Jouez ces numéros </u> sur votre prochain billet de loto <u> </u> (nous ne vendons pas les billets)',
    'step_7' => 'Que la fortune soit de votre côté! Nous espérons avoir de vos nouvelles et vous souhaitons <u> bonne chance! </u>',

];
