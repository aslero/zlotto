<?php

return [
    'login' => 'S\'identifier',
    'first_name' => 'Prénom',
    'last_name' => 'Nom de famille',
    'birth' => 'Date de naissance',
    'email' => 'Email',
    'password' => 'Mot de passe',
    'new_password' => 'Nouveau mot de passe',
    'confirm_password' => 'Confirmer le nouveau mot de passe',
    'mail_notif' => 'Acceptez de recevoir des notifications par courrier',
    'save' => 'Mettre à jour le profil',
];
