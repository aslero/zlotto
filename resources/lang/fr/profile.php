<?php

return [
    'billing' => 'Facturation',
    'billing_t' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmo',
    'my_numbers' => 'Mes numéros gagnants',
    'pay_history' => 'historique de paiement',
    'family_numbers' => 'Membres de la famille ajoutés',
    'family_numbers_t' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod',
];
