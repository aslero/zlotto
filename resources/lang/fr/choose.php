<?php

return [
    'title' => 'Votre quiz sur les numéros porte-bonheur',
    'c_region' => 'CHOISISSEZ LA RÉGION',
    'c_region_t' => 'Il existe de nombreuses variantes de passages de lorem Ipsum disponibles majoritaires',
    'c_lottery' => 'CHOISISSEZ LA LOTERIE',
    'c_country' => 'Choisissez un pays',
    'c_country_t' => 'Choisissez un pays',
    'answer_question' => 'RÉPONDRE À LA QUESTION',
    'submit' => 'Soumettre',
    'index_num' => 'Voulez-vous augmenter vos chances de gagner à la loterie en utilisant la science ancienne de la <u> numérologie </u>'
];
