<?php

return [
    'step_1' => 'Liituge <br/> <u> Zlotto kontoga </u>',
    'step_2' => 'Vastake isikupärastatud küsimustikule ja teie <u> elu </u> olulistele sündmustele',
    'step_3' => '<u> Makske pisike </u> liikmemaks (see aitab Zlottodel äri jätkata!)',
    'step_4' => '<u> Valige oma lemmik </u> loteriimark, kuupäev, kellaaeg ja see, kui palju te mängite',
    'step_5' => 'Meie numeroloogiaeksperdid <u> arvutavad teie elu põhjal välja </u> teie kordumatu <u> numbrite komplekti </u> <span>! </span>',
    'step_6' => '<u> Mängige neid numbreid </u> oma järgmisel loto <u> piletil </u> (me ei müü pileteid)',
    'step_7' => 'Las õnn on teie poolel! Loodame sinult kuulda ja soovime teile <u> õnne! </u>',

];
