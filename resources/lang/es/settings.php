<?php

return [
    'login' => 'Logi sisse',
    'first_name' => 'Eesnimi',
    'last_name' => 'Perekonnanimi',
    'birth' => 'Sünnikuupäev',
    'email' => 'E-post',
    'password' => 'Parool',
    'new_password' => 'Uus salasõna',
    'confirm_password' => 'Kinnita Uus salasõna',
    'mail_notif' => 'Nõustute e-kirjade teatiste saamisega',
    'save' => 'Uuenda profiili',
];
