<?php

return [
    'learn_more' => 'Lisateave',
    'create_account' => 'Loo konto',
    'terms' => 'Tingimused',
    'byu_now' => 'OSTA KOHE',
    'how_it_works' => 'Kuidas see töötab',
    'increse' => 'Suurendage nüüd oma võimalust',
    'scroll' => 'Kerige allapoole ja lisateavet',
];
