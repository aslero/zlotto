<?php

return [
    'title' => 'Makse',
    'discount' => 'allahindlus',
    'p_year' => 'Aastas',
    'month' => 'kuu',
    'total' => 'Kokku',
    'cvv' => 'cvv',
    'error_cvv' => 'Vale cvv',
    'expiration_date' => 'aegumiskuupäev',
    'error_expiration_date' => 'Vale aegumiskuupäev',
    'card_number' => 'Kaardi number',
    'error_card_number' => 'Vale kaardi number',
    'tariff' => 'Tariif',
    'members' => 'Liikmed',
    'add_f_m' => 'Lisage pereliige',
    'add_f_d' => 'Kas soovite oma perega mängida? Andke neile võimalus oma õnnenumbrite genereerimiseks. Võtke seda paketti lisandmoodulina 1,2,3 või 4 liikmele!',
    'unlimited' => 'Mängige kuu aega piiramatult. (1 KUU)',
    'm_package' => 'MEIE KUU KUUPAKK',
    'description' => 'Meie Zlotto kogukonna kõige populaarsem aastane liikmelisuse pakett. Genereerige TÄISAASTA jaoks piiramatu arvu õnnenumbreid! (12 KUUD)',

];
