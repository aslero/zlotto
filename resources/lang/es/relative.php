<?php

return [
    'name' => 'Nimi',
    'email' => 'E-post',
    'date' => 'Kuupäev',
    'status' => 'Olek',
    'send' => 'Saada kutse',
    'my_relative' => 'Teie kutsed',
    'description' => 'Kontrollige, kas teie sugulasel võib olla rohkem õnne kui teil. Kas soovite sugulast lisada? -> Kas soovite mängida oma pere või sugulastega? Vaadake, kas nad on õnnelikumad kui sina! Lisage siia sugulane.',
];
