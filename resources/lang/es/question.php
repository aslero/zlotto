<?php

return [
    'next'   => 'Järgmine',
    'finish'   => 'Lõpetama',
    '3_desc'   => 'Lülitage kalkulaator sisse. Te peate numbreid summeerima, kuni saate numbri vahemikus 0 kuni 9.<br>Näide: 1234567 - 1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 = 36; 3 + 6 = 9 - kasutage arvu 9.',
];
