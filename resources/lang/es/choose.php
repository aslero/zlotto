<?php

return [
    'title' => 'Teie isiklike õnnenumbrite viktoriin',
    'c_region' => 'VALI PIIRKOND',
    'c_region_t' => 'Ipsum lorem saadaval olevates lõikudes on palju variante',
    'c_lottery' => 'VALI LOTERII',
    'c_country' => 'Valige riik',
    'c_country_t' => 'Valige riik',
    'answer_question' => 'VASTA KÜSIMUSELE',
    'submit' => 'Esita',
    'index_num' => 'Kas soovite suurendada oma võimalust loteriil võita, kasutades iidset teadust <u> numeroloogia </u>'
];
