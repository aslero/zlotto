<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'nav' => [
        'index' => 'Peamine',
        'choose_lottery' => 'VALI LOTERII',
        'about' => 'KUIDAS SEE TÖÖTAB',
        'price' => 'HINDAMINE',
    ],
    'profile' => [
        'question' => 'Minu õnnenumbrid',
        'add_relative' => 'Lisage sugulane',
        'past_number' => 'Varasemad numbrid',
        'settings' => 'Seadistused',
        'profile' => 'Minu konto',
        'logout' => 'Logi välja',
    ],
];
