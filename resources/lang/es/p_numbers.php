<?php

return [
    'lottery' => 'Loterii',
    'date' => 'Kuupäev',
    'time' => 'Aeg',
    'w_numbers' => 'Võidunumbrid',
    'choose' => 'Vali loterii',
    'l_numbers' => 'Viimati genereeritud õnnenumbrid',
    'show' => 'Näita',
];
