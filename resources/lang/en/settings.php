<?php

return [
    'login' => 'Login',
    'first_name' => 'First Name',
    'last_name' => 'Last Name',
    'birth' => 'Date of birth',
    'email' => 'Email',
    'password' => 'Password',
    'new_password' => 'New password',
    'confirm_password' => 'Confirm new password',
    'mail_notif' => 'Agree to receive mail notifications',
    'save' => 'Update profile',
];
