<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'nav' => [
      'index' => 'Main',
      'choose_lottery' => 'CHOOSE LOTTERY',
      'about' => 'HOW IT WORKS',
      'price' => 'PRICING',
    ],
    'profile' => [
        'question' => 'My Lucky Numbers',
        'add_relative' => 'Add a Relative',
        'past_number' => 'Past Numbers',
        'settings' => 'Settings',
        'profile' => 'My Account',
        'logout' => 'Log out',
    ],
];
