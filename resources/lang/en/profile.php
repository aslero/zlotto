<?php

return [
    'billing' => 'Billing',
    'billing_t' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmo',
    'my_numbers' => 'My winning numbers',
    'pay_history' => 'Payment History',
    'family_numbers' => 'Family members added',
    'family_numbers_t' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod',
];
