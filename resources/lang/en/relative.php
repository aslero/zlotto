<?php

return [
    'name' => 'Name',
    'email' => 'Email',
    'date' => 'Date',
    'status' => 'Status',
    'send' => 'Send invitation',
    'my_relative' => 'Your invitations',
    'description' => 'Check if your relative can be more lucky to win than you. Want to add a relative? -> Want to play with your family or relatives? See if they are luckier than you are! Add a relative here.',
];
