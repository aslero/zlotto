<?php

return [
    'title' => 'Your Personal Lucky Numbers’ Quiz',
    'c_region' => 'CHOOSE REGION',
    'c_region_t' => 'There are many variations of passages of lorem Ipsum available majority',
    'c_lottery' => 'CHOOSE LOTTERY',
    'c_country' => 'Choose country',
    'c_country_t' => 'Choose country',
    'answer_question' => 'ANSWER THE QUESTION',
    'submit' => 'Submit',
    'index_num' => 'Do you want to increase your chance of winning the lottery using the ancient science of <u>numerology</u>'
];
