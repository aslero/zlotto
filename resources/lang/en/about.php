<?php

return [
    'step_1' => 'Sign up for a <br/><u>Zlotto account</u>',
    'step_2' => 'Answer a personalized questionnaire tailored to you and the important events in your <u>life</u>',
    'step_3' => '<u>Pay a tiny</u> membership fee (it helps Zlotto stay in business!)',
    'step_4' => '<u>Pick your favourite</u> lottery brand, date, time and how much you\'re going to be playing',
    'step_5' => 'Our numerology experts <u>will calculate</u> your unique set of <u>numbers</u> <span>to play based on your life!</span>',
    'step_6' => '<u>Play these numbers</u> on your next lotto <u>ticket</u> (we do not sell the tickets)',
    'step_7' => 'Let the fortune be on your side! We hope to hear from you and wish you <u>luck!</u>',

];
