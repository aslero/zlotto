<?php

return [
    'next'   => 'Next',
    'finish'   => 'Finish',
    '3_desc'   => 'Turn on the calculator, please. You need to sum numbers until you get a number from 0 to 9. <br>Example: 1234567 — 1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 = 36; 3 + 6 = 9 — Use the number 9.',
];
