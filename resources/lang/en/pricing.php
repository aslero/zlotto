<?php

return [
    'title' => 'Payment',
    'discount' => 'discount',
    'p_year' => 'Per year',
    'month' => 'month',
    'total' => 'Total',
    'cvv' => 'cvv',
    'error_cvv' => 'Incorrect cvv',
    'expiration_date' => 'expiration date',
    'error_expiration_date' => 'Incorrect expiration date',
    'card_number' => 'Card number',
    'error_card_number' => 'Incorrect card number',
    'tariff' => 'Tariff',
    'members' => 'Members',
    'add_f_m' => 'Add family member',
    'add_f_d' => 'Want to play with your family? Give them a chance to generate their own lucky numbers. Take this package as an add-on for 1,2,3 or 4 members!',
    'unlimited' => 'Play unlimited for a month. (1 MONTH)',
    'm_package' => 'OUR MONTHLY PACKAGE',
    'description' => 'Our most popular annual membership package from the Zlotto community. Generate unlimited lucky numbers for a FULL YEAR! (12 MONTHS)',

];
