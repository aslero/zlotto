<?php

return [
    'lottery' => 'Lottery',
    'date' => 'Date',
    'time' => 'Time',
    'choose' => 'choose lottery',
    'w_numbers' => 'Winning numbers',
    'l_numbers' => 'Last lucky numbers generated for',
    'show' => 'Show',
];
